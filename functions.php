<?php
/**
 * Functions
 *
 * @package      Onja
 * @author       Georgius Fransnico
 * @since        1.0.0
 * @license      GPL-2.0+
**/

// Starts the engine.
//require_once get_template_directory() . '/lib/init.php';

// Defines the child theme (do not remove).
define( 'CHILD_THEME_NAME', 'Onja' );
define( 'CHILD_THEME_URL', 'https://onja.org/' );
define( 'CHILD_THEME_VERSION', filemtime( get_stylesheet_directory() . '/assets/dist/css/app.css' ) );

/**
 * Theme setup.
 *
 * Attach all of the site-wide functions to the correct hooks and filters. All
 * the functions themselves are defined below this setup function.
 *
 * @since 1.0.0
 */
function gtw_child_theme_setup() {

    // General cleanup
	//include_once( get_stylesheet_directory() . '/lib/general/wordpress-cleanup.php' );
    include_once( get_stylesheet_directory() . '/lib/general/genesis-changes.php' );

    // Functions
    include_once( get_stylesheet_directory() . '/lib/functions/helpers.php' );
    include_once( get_stylesheet_directory() . '/lib/functions/theme-options.php' );

    // Gutenberg Support
    include_once( get_stylesheet_directory() . '/lib/gutenberg/gutenberg.php' );

    // Customizer
    include_once( get_stylesheet_directory() . '/lib/customizer/customizer.php' );

    // Modules
    include_once( get_stylesheet_directory() . '/lib/modules/init.php' );

    // Shortcodes
    include_once( get_stylesheet_directory() . '/lib/shortcodes/onja_recent_posts.php' );

    // Structure
    include_once( get_stylesheet_directory() . '/lib/structure/defaults.php' );
    include_once( get_stylesheet_directory() . '/lib/structure/header.php' );
    include_once( get_stylesheet_directory() . '/lib/structure/menu.php' );
    include_once( get_stylesheet_directory() . '/lib/structure/footer.php' );

    // Content Management
    include_once( get_stylesheet_directory() . '/lib/content-management/content-management.php' );

    include_once( get_stylesheet_directory() . '/lib/functions/enqueue.php' );


}
add_action( 'genesis_setup', 'gtw_child_theme_setup', 15 );