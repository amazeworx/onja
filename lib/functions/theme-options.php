<?php
/**
 * Theme Options
 *
 * @package      Onja
 * @author       Georgius Fransnico
 * @since        1.0.0
 * @license      GPL-2.0+
**/

// * Add Options Page
if ( function_exists( 'acf_add_options_page' ) ) {

	// add parent
	$parent = acf_add_options_page(
		array(
			'page_title' => 'Theme General Settings',
			'menu_title' => 'Theme Settings',
			'redirect'   => false,
		)
	);

		// add sub page
		acf_add_options_sub_page(
			array(
				'page_title'  => 'Archive Settings',
				'menu_title'  => 'Archives',
				'parent_slug' => $parent['menu_slug'],
			)
		);

		// add sub page
		acf_add_options_sub_page(
			array(
				'page_title'  => 'Content Sections',
				'menu_title'  => 'Content Sections',
				'parent_slug' => $parent['menu_slug'],
			)
		);

}