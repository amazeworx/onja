<?php
/**
 * Enqueue
 *
 * @package      Onja
 * @author       Georgius Fransnico
 * @since        1.0.0
 * @license      GPL-2.0+
**/

/**
 * Global enqueues
 *
 * @since  1.0.0
 * @global array $wp_styles
 */
function onja_global_enqueues() {

    // javascript
	//wp_enqueue_script( 'polyfill', '//polyfill.io/v3/polyfill.js?features=es5,es6,es7,fetch&flags=gated' );
    wp_enqueue_script( 'polyfill', 'https://polyfill.io/v3/polyfill.min.js?features=es2015%2Ces2016%2Ces5%2Ces6%2Ces7%2Cfetch%2Ces2017' );
    wp_enqueue_script( 'mmenu', get_stylesheet_directory_uri() . '/assets/dist/js/mmenu.js', NULL, filemtime( get_stylesheet_directory() . '/assets/dist/js/mmenu.js' ), true );
    wp_enqueue_script( 'mmenu-polyfills', get_stylesheet_directory_uri() . '/assets/dist/js/mmenu.polyfills.js', NULL, filemtime( get_stylesheet_directory() . '/assets/dist/js/mmenu.polyfills.js' ), true );
    wp_enqueue_script( 'fancybox', get_stylesheet_directory_uri() . '/assets/dist/js/jquery.fancybox.min.js', array( 'jquery' ), '3.5.7', true );
    wp_enqueue_script( 'swiper', get_stylesheet_directory_uri() . '/assets/dist/js/swiper-bundle.min.js', array( 'jquery' ), '6.4.11', true );

    wp_enqueue_script( 'onja-mmenu', get_stylesheet_directory_uri() . '/assets/dist/js/onja-mmenu.js', array( 'mmenu' ), filemtime( get_stylesheet_directory() . '/assets/dist/js/onja-mmenu.js' ), true );
    wp_enqueue_script( 'onja-swiper', get_stylesheet_directory_uri() . '/assets/dist/js/onja-swiper.js', array( 'swiper' ), filemtime( get_stylesheet_directory() . '/assets/dist/js/onja-swiper.js' ), true );
	//wp_enqueue_script( 'onja-responsive-menu', get_stylesheet_directory_uri() . "/assets/dist/js/responsive-menus.min.js", array( 'jquery' ), '1.0.0', true );
	//wp_localize_script( 'onja-responsive-menu', 'genesis_responsive_menu', onja_responsive_menu_settings() );
    wp_enqueue_script( 'onja-app', get_stylesheet_directory_uri() . '/assets/dist/js/app.js', array( 'jquery' ), filemtime( get_stylesheet_directory() . '/assets/dist/js/app.js' ), true );

	// css
    wp_dequeue_style( 'child-theme' );
    wp_enqueue_style( 'nunito-sans', 'https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap' );
    wp_enqueue_style( 'material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons' );
    wp_enqueue_style( 'mmenu', get_stylesheet_directory_uri() . '/assets/dist/css/mmenu.css', array(), filemtime( get_stylesheet_directory() . '/assets/dist/css/mmenu.css' ) );
    wp_enqueue_style( 'swiper', get_stylesheet_directory_uri() . '/assets/dist/css/swiper-bundle.min.css', array(), '6.4.11' );
    wp_enqueue_style( 'onja-style', get_stylesheet_directory_uri() . '/assets/dist/css/app.css', array(), filemtime( get_stylesheet_directory() . '/assets/dist/css/app.css' ) );

}
add_action( 'wp_enqueue_scripts', 'onja_global_enqueues', 999 );

function onja_dequeue_scripts() {
    if ( ! is_user_logged_in() ) {
        wp_deregister_style( 'dashicons' );
    }
}
add_action( 'wp_enqueue_scripts', 'onja_dequeue_scripts' );