<?php
/**
 * Customizer.
 *
 * @package      Onja
 * @author       Georgius Fransnico
 * @since        1.0.0
 * @license      GPL-2.0+
 */

// add_theme_support(
//     'genesis-custom-logo',
//     [
//         'height'      => 42,
//         'width'       => 129,
//         'flex-height' => true,
//         'flex-width'  => true,
//     ]
// );