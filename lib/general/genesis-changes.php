<?php
/**
 * Genesis Changes
 *
 * @package      Onja
 * @author       Georgius Fransnico
 * @since        1.0.0
 * @license      GPL-2.0+
**/

// Theme Supports
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
add_theme_support( 'genesis-responsive-viewport' );

// Remove admin bar styling
add_theme_support( 'admin-bar', array( 'callback' => '__return_false' ) );

// Add Genesis Menus
add_theme_support(
	'genesis-menus',
	array(
		'primary'   => __( 'Header Menu', 'onja' ),
        'footer-menu' => __( 'Footer Menu', 'onja' ),
        'header-button' => __( 'Header Button', 'onja' ),
	)
);

// Don't enqueue child theme stylesheet
remove_action( 'genesis_meta', 'genesis_load_stylesheet' );

// Remove Edit link
add_filter( 'genesis_edit_post_link', '__return_false' );

// Remove Genesis Favicon (use site icon instead)
remove_action( 'wp_head', 'genesis_load_favicon' );

// Add custom thumbnail size
add_image_size( 'sidebar-featured', 75, 75, true );
add_image_size( 'blog', 480, 350, true );
add_image_size( 'post-featured', 1170, 520, true );
add_image_size( 'student-featured', 430, 520, true );

// Add support for 3-column footer widgets
add_theme_support( 'genesis-footer-widgets', 3 );