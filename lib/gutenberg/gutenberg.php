<?php
/**
 * Gutenberg Support
 *
 * @package      Onja
 * @author       Georgius Fransnico
 * @since        1.0.0
 * @license      GPL-2.0+
**/

// Add support for editor styles.
add_theme_support( 'editor-styles' );

// Enqueue editor styles.
add_editor_style( get_stylesheet_directory_uri() . '/lib/gutenberg/editor-styles.css' );

// Adds support for block alignments.
add_theme_support( 'align-wide' );

// Make media embeds responsive.
add_theme_support( 'responsive-embeds' );

// Adds support for editor font sizes.
add_theme_support(
	'editor-font-sizes',
	genesis_get_config( 'editor-font-sizes' )
);

// Adds support for editor color palette.
add_theme_support(
	'editor-color-palette',
	genesis_get_config( 'editor-color-palette' )
);

// Add custom blocks
add_action('acf/init', 'onja_acf_init_block_types');
function onja_acf_init_block_types() {

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

        // register feature block
        acf_register_block(
            array(
                'name'            => 'feature',
                'title'           => __( 'Onja Feature' ),
                'description'     => __( 'A custom feature block.' ),
                'render_callback' => 'templates/blocks/feature.php',
                'category'        => 'formatting',
                'icon'            => 'lightbulb',
                'keywords'        => array( 'feature', 'icon' ),
            )
        );

    }
}