<?php
/**
 * Site Header
 *
 * @package      Onja
 * @author       Georgius Fransnico
 * @since        1.0.0
 * @license      GPL-2.0+
**/

/** Add Logo */
function onja_do_header_logo() {
    $home_url = home_url();
    $header_theme = get_field( 'header_theme' );
    if (($header_theme == 'dark') || ($header_theme == 'transparent')) {
        $logo_src = get_field( 'white_logo', 'option' );
    } else {
        $logo_src = get_field( 'default_logo', 'option' );
    }
    if ($logo_src) {
        echo '<div class="header-logo">';
            echo '<a href="' . $home_url . '" class="header-logo--link">';
                echo '<img src="' . $logo_src . '" class="header-logo--img">';
            echo '</a>';
        echo '</div>';
    }
}
add_action( 'genesis_site_title', 'onja_do_header_logo', 9 );

/** Add Site Header Class */
function onja_add_site_header_css_attr( $attributes ) {

    $header_theme = get_field( 'header_theme' );

    // add original plus extra CSS classes
    $attributes['class'] .= ' ' . $header_theme;

    // return the attributes
    return $attributes;

}
add_filter( 'genesis_attr_site-header', 'onja_add_site_header_css_attr' );