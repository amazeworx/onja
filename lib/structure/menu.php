<?php
/**
 * Defines responsive menu settings.
 *
 * @since 2.3.0
 */
function onja_responsive_menu_settings() {

	$settings = array(
		'mainMenu'         => __( 'Menu', 'onja-pro' ),
		'menuIconClass'    => 'icon-menu',
		'subMenu'          => __( 'Submenu', 'onja-pro' ),
		'subMenuIconClass' => 'icon-angle-down',
		'menuClasses'      => array(
			'combine' => array(
				'.nav-primary',
			),
			'others'  => array(),
		),
	);

	return $settings;

}

/** Move Primary Nav Menu to Header */
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_header', 'genesis_do_nav' );

/** Add Header Button Menu */
function onja_do_header_button() {
    echo wp_nav_menu( array(
        'theme_location' => 'header-button',
        'container_class' => 'header-button'
    ) );
}
add_action( 'genesis_header', 'onja_do_header_button', 13 );

// Add Mobile Menu Button
function onja_add_mobile_menu_button() {
    echo '<a href="#onja-menu" class="onja-menu-toggle inline-block lg:hidden"><span class="material-icons">menu</span></a>';
}
add_action( 'genesis_header', 'onja_add_mobile_menu_button', 10 );

add_filter( 'genesis_attr_nav-primary', 'onja_primary_nav_id' );
function onja_primary_nav_id( $attributes ) {
    $attributes['id'] = 'onja-menu';
    return $attributes;
}