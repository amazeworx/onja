<?php
/**
 * Site Footer
 *
 * @package      Onja
 * @author       Slant Agency
 * @since        1.0.0
 * @license      GPL-2.0+
**/

remove_action( 'genesis_footer', 'genesis_do_footer' );

function onja_footer_subscribe() {
    echo '<div class="bg-crayola py-12 md:py-8 xl:py-10">';

        echo '<div class="container mx-auto px-4 lg:px-6 xl:px-8">';
            echo '<div class="flex flex-wrap items-center text-white text-center md:text-left">';
                echo '<div class="w-full mb-8 md:w-auto md:mb-0 md:flex-none"><h4 class="text-2xl text-bold xl:text-2xl">Stay in the loop</h4></div>';
                echo '<div class="subscribe_popup_form flex flex-wrap w-full justify-center md:w-auto md:flex-1 md:flex-no-wrap md:ml-8 md:justify-start">';
                    echo '<input class="w-full mb-6 pb-2 text-center bg-transparent text-white text-xl border-b border-white placeholder-white placeholder-opacity-80 focus:placeholder-opacity-40 focus:outline-none focus:shadow-none md:mb-0 md:pb-0 md:text-xl md:text-left" type="email" name="newsletter_email" id="newsletter_email"  placeholder="Your Email">';
                    echo '<button id="onja_subscribe_btn" type="button" data-fancybox data-src="#onja_popup--newsletter" class="JS_subscribe_popup flex-none py-2 px-6 rounded-full bg-white text-crayola text-lg tracking-wide md:ml-4 md:text-xl md:py-3 md:px-8 xl:text-2xl xl:ml-8">Subscribe</button>';
                echo '</div>';
                echo do_shortcode( '[onja_subscribe_popup]' );
            echo '</div>';
        echo '</div>';

    echo '</div>';
}
add_action( 'genesis_before_footer', 'onja_footer_subscribe' );

function onja_footer_widgets() {
    echo '<div class="footer-widgets bg-gunmetal py-8 xl:py-16">';

        echo '<div class="container mx-auto px-4 lg:px-6 xl:px-8">';

            echo '<div class="flex flex-wrap text-white xl:-mx-8">';
                echo '<div class="flex flex-wrap w-full lg:w-3/5 xl:px-8">';
                    echo '<div class="flex flex-col w-full mb-8 md:mb-0 md:w-1/2 md:pr-6 xl:pr-8">';
                        echo '<div class="block mb-4 xl:mb-8">';
                            $logo_src = get_field( 'white_logo', 'option' );
                            if ($logo_src) {
                                echo '<img class="mb-6" src="' . $logo_src . '" width="129" height="42">';
                            }
                            $about_text = get_field( 'about_text', 'option' );
                            if ($about_text) {
                                echo '<p class="text-base xl:text-lg">' . $about_text . '</p>';
                            }
                        echo '</div>';
                        echo '<div class="block mt-auto">';
                            echo '<a class="inline-block text-white mr-4" href="https://www.facebook.com/OnjaAfrica/" target="_blank">';
                                echo gs_icon( array( 'icon' => 'facebook', 'group' => 'social', 'size' => 32, 'class' => 'fill-current text-white' ) );
                            echo '</a>';
                            echo '<a class="inline-block text-white mr-4" href="https://www.instagram.com/onja_madagascar/" target="_blank">';
                                echo gs_icon( array( 'icon' => 'instagram', 'group' => 'social', 'size' => 32, 'class' => 'fill-current text-white' ) );
                            echo '</a>';
                            echo '<a class="inline-block text-white" href="https://www.linkedin.com/company/onja" target="_blank">';
                                echo gs_icon( array( 'icon' => 'linkedin', 'group' => 'social', 'size' => 32, 'class' => 'fill-current text-white' ) );
                            echo '</a>';
                        echo '</div>';
                    echo '</div>';

                    genesis_widget_area( 'footer-2', array(
                        'before' => '<div class="w-full mb-8 md:mb-0 md:w-1/2 md:pl-6 xl:pl-8">',
                        'after'  => '</div>',
                    ) );

                echo '</div>';

                genesis_widget_area( 'footer-3', array(
                    'before' => '<div class="w-full md:mt-12 lg:mt-0 lg:w-2/5 xl:px-8">',
                    'after'  => '</div>',
                ) );

            echo '</div>';

        echo '</div>';

    echo '</div>';
}
add_action( 'genesis_before_footer', 'onja_footer_widgets' );

function onja_footer_creds() {
    $credits_text = get_field( 'credits_text', 'option' );
    if ($credits_text) {
        echo '<div class="bg-gunmetal text-white pb-8 xl:pb-16">';

            echo '<div class="container mx-auto px-4 lg:px-6 xl:px-8">';

                echo '<div class="flex border-white border-t pt-4">';
                    echo '<span class="inline-block ml-auto">';
                        echo $credits_text;
                    echo '</span>';
                echo '</div>';

            echo '</div>';

        echo '</div>';
    }
}
add_action( 'genesis_footer', 'onja_footer_creds' );