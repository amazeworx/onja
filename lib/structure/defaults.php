<?php
/**
 * Theme Defaults
 *
 * @package      Onja
 * @author       Slant Agency
 * @since        1.0.0
 * @license      GPL-2.0+
**/

//* Add custom body class to the head
//add_filter( 'body_class', 'gtw_body_class' );
function gtw_body_class( $classes ) {

	$classes[] = 'prose max-w-none lg:prose-lg xl:prose-xl';
	return $classes;

}