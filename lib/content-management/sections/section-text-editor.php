<?php

function onja_do_section_text_editor() {

    include( get_stylesheet_directory() . '/lib/content-management/components/section-settings.php');
    include( get_stylesheet_directory() . '/lib/content-management/components/headline.php');
    include( get_stylesheet_directory() . '/lib/content-management/components/subheadline.php');

    $boxed_content = get_sub_field( 'boxed_content' );
    if ( $boxed_content ) {
        $boxed_content = 'bg-white p-6 md:p-12 xl:p-16';
    }

    $text_editor = get_sub_field( 'text_editor' );

    echo '<section id="' . esc_attr( $section_anchor_id ) . '" class="section-text-editor px-4 lg:px-6 xl:px-8 ' . $vertical_padding . '" style="background-color: ' . $background_color . '">';
        echo '<div class="container mx-auto">';
            echo '<div class="mx-auto ' . $boxed_content . '" style="max-width: ' .  $content_max_width . '">';
                if ($headline_title) {
                    echo '<div class="mb-8">';
                        echo '<h3 class="' . $headline_alignment . ' ' . $headline_title_size . ' mb-4 font-extrabold" style="color: ' . $headline_title_color . ';">' . $headline_title . '</h3>';
                        if ($subheadline) {
                            echo '<div class="' . $subheadline_alignment . ' ' . $subheadline_size . ' " style="color: ' . $subheadline_color . '">';
                                echo $subheadline_title;
                            echo '</div>';
                        }
                    echo '</div>';
                }

                echo $text_editor;

            echo '</div>';
        echo '</div>';
    echo '</section>';
}