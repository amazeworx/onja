<?php

function onja_do_section_hero() {
    $hero_title = ( get_sub_field( 'hero_title' ) ) ?: get_the_title();
    $hero_title = apply_filters( 'onja_hero_title', $hero_title );

    $hero_title_tag = get_sub_field( 'hero_title_tag' );

    $hero_content = get_sub_field( 'hero_content' );
    $hero_content = apply_filters( 'onja_hero_content', $hero_content );

    $button_text = get_sub_field( 'button_text' );
    $button_url = get_sub_field( 'button_url' );

    $background_image = ( get_sub_field( 'background_image' ) ) ?: get_field( 'default_splash', 'options' );

    $overlay = get_sub_field( 'overlay' );

    $height_class = ( 'screen' === get_sub_field( 'height' ) ) ? 'h-screen' : 'h-auto';
    $boxed_class = ( get_sub_field( 'boxed_content' ) ) ? 'bg-black bg-opacity-50 rounded-2xl p-6 md:p-8' : '';

    $content_position = get_sub_field( 'content_position' );
    if ($content_position == 'left' ) {
        $content_position_class = 'w-full mr-auto md:w-1/2 xl:w-1/3';
    } else if ( $content_position == 'center' ) {
        $content_position_class = 'text-center max-w-screen-sm mx-auto';
    } else { // right
        $content_position_class = 'w-full ml-auto md:w-1/2 xl:w-1/3';
    }

    echo '<section class="section-hero relative flex items-center pt-28 pb-20 px-4 text-white bg-gunmetal bg-center bg-no-repeat bg-cover md:pt-40 md:pb-20 lg:px-6 xl:px-8 ' . esc_attr( $height_class ) . '" style="background-image:url(' . esc_url( $background_image ) . ')">';

        if ($overlay) {
            echo '<div class="overlay bg-crayola bg-opacity-80 z-0 w-full h-full absolute top-0 left-0 right-0 bottom-0"></div>';
        }

        echo '<div class="container mx-auto z-10 relative flex flex-row flex-wrap">';
            echo '<div class="hero-content ' . esc_attr( $boxed_class ) . ' ' . esc_attr( $content_position_class ) . '">';
                printf( '<%s class="hero-title text-4xl leading-none text-white font-black mb-4 md:text-5xl md:mb-4">%s</%s>', esc_attr( $hero_title_tag ), esc_html( $hero_title ), esc_attr( $hero_title_tag ) );
                if ( ! empty( $hero_content ) ) {
                    echo '<div class="hero-content font-medium text-xl leading-snug md:text-2xl">' . $hero_content . '</div>';
                }
                if ( $button_url && $button_text ) {
                    echo '<div class="hero-button mt-4 md:mt-7">';
                        echo '<a href="' . esc_url( $button_url ) . '" class="py-2 px-6 bg-white inline-block rounded-full no-underline text-xl leading-8 tracking-wide font-semibold text-gray-600 transition hover:bg-cerulean hover:text-white mb-2 md:py-3 md:px-9 md:text-2xl">' . $button_text . '</a>';
                    echo '</div>';
                }
            echo '</div>';
        echo '</div>';

    echo '</section>';
}