<?php

function onja_do_section_icon_card() {

    $background_color = get_sub_field( 'background_color' );
    $vertical_padding = get_sub_field( 'vertical_padding' );

    if ( $vertical_padding == 'sm' ) {
        $vertical_padding = 'py-4 lg:py-6 xl:py-8';
    } else if ( $vertical_padding == 'md' ) {
        $vertical_padding = 'py-6 lg:py-8 xl:py-12';
    } else if ( $vertical_padding == 'lg' ) {
        $vertical_padding = 'py-8 lg:py-12 xl:py-16';
    } else if ( $vertical_padding == 'xl' ) {
        $vertical_padding = 'py-10 lg:py-16 xl:py-20';
    } else if ( $vertical_padding == '2xl' ) {
        $vertical_padding = 'py-12 lg:py-16 xl:py-24';
    } else if ( $vertical_padding == '3xl' ) {
        $vertical_padding = 'py-16 lg:py-20 xl:py-28';
    } else if ( $vertical_padding == '4xl' ) {
        $vertical_padding = 'py-16 lg:py-24 xl:py-32';
    } else {
        $vertical_padding = 'py-8 lg:py-12 xl:py-16';
    }

    $content_max_width = get_sub_field( 'content_max_width' );
    $section_anchor = get_sub_field( 'section_anchor' );
    $section_anchor_id = get_sub_field( 'section_anchor_id' );
    $section_heading = get_sub_field( 'section_heading' );
    $learn_more_button = get_sub_field( 'learn_more_button' );

    echo '<section id="' . esc_attr( $section_anchor_id ) . '" class="px-4 md:px-6 lg:px-6 xl:px-8 ' . $vertical_padding . '"style=" background-color: ' . $background_color . '>';
        echo '<div class="container mx-auto">';

            echo '<div class="mx-auto" style="max-width: ' .  $content_max_width . '">';

                if ( $section_heading ) {
                    echo '<h2 class="text-3xl text-center text-cerulean font-extrabold mb-16 md:text-5xl">' . $section_heading . '</h2>';
                }

                if( have_rows('icon_card') ):

                    echo '<div class="icon-card-wrap flex flex-wrap mt-20 justify-center md:mt-28 lg:flex-no-wrap lg:mt-32">';

                        // Loop through rows.
                        while( have_rows('icon_card') ) : the_row();

                            // Load sub field value.
                            $icon_title = get_sub_field('icon_title');
                            $icon_image = get_sub_field('icon_image');
                            $icon_description = get_sub_field('icon_description');
                            $status = get_sub_field('status');

                            if ($status) {

                                echo '<div class="bg-aliceblue rounded-xl mx-auto mb-20 w-full md:max-w-xs md:mx-4 lg:w-1/3">';
                                    echo '<div class="text-center -mt-12 mx-auto">';
                                        echo '<img class="mx-auto" src="' . esc_url( $icon_image ) . '" width="96">';
                                    echo '</div>';
                                    echo '<div class="text-center p-6 pb-12 md:p-10 md:pb-16">';
                                        echo '<h4 class="text-lg font-extrabold mb-6 md:mb-8">' . $icon_title . '</h4>';
                                        echo '<p class="font-light">' . $icon_description . '</p>';
                                    echo '</div>';
                                echo '</div>';

                            }

                        // End loop.
                        endwhile;

                    echo '</div>';

                // No value.
                else :
                    // Do something...
                endif;

                if ( $learn_more_button['button_url']['url'] ) {
                    echo '<div class="mx-auto mt-0 text-center"><a href="' . $learn_more_button['button_url']['url'] . '" class="inline-block text-center text-cerulean text-xl font-normal leading-none hover:text-crayola md:text-2xl"><span class="inline-block align-top leading-none mr-1">' . $learn_more_button['button_text'] . '</span><span class="material-icons align-top">chevron_right</span></a></div>';
                }


            echo '</div>';

        echo '</div>';

    echo '</section>';
}