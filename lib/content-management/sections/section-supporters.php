<?php

function onja_do_section_supporters() {

    include( get_stylesheet_directory() . '/lib/content-management/components/section-settings.php');
    include( get_stylesheet_directory() . '/lib/content-management/components/headline.php');

    echo '<section class="bg-cover bg-top bg-no-repeat relative px-4 md:px-6 lg:px-6 xl:px-8 ' . $vertical_padding . '" style="background-image: url(' . $background_image . ')">';

        //echo $background_overlay;
        if ($background_overlay) {
            echo '<div class="overlay opacity-80 z-0 w-full h-full absolute top-0 left-0 right-0 bottom-0" style="background-color: ' . $background_overlay . '"></div>';
        }

        echo '<div class="container mx-auto relative z-10">';

            if ($headline_title) {
                echo '<h2 class="text-white font-black mb-12 md:mb-16 ' . $headline_alignment . ' ' . $headline_title_size . '" style="color: ' . $headline_title_color . '">' . $headline_title . '</h2>';
            }

            // Check rows exists.
            if( have_rows('supporter_list') ):

                echo '<div class="supporter-list flex flex-wrap items-center justify-center -mx-4 lg:flex-no-wrap">';

                // Loop through rows.
                while( have_rows('supporter_list') ) : the_row();

                    // Load sub field value.
                    $image_or_text = get_sub_field('image_or_text');
                    $image = get_sub_field( 'image' );
                    $image_url = $image['image_upload'];
                    $image_link = $image['link'];
                    $text_area = get_sub_field( 'text' );

                    if ($image_or_text !== TRUE) {
                        echo '<div class="h-full flex flex-col items-center justify-center text-center w-1/2 px-4 mb-8 lg:w-auto lg:px-12 xl:px-16">';
                            echo '<a class="h-full flex items-center" href="' . $image_link . '"><img class="inline-block object-contain" src="' . $image_url . '"></a>';
                        echo '</div>';
                    } else if ($image_or_text === TRUE ) {
                        echo '<div class="textlink h-full flex flex-col items-center justify-center text-center w-1/2 px-4 mb-8 lg:w-auto lg:px-12 xl:px-16">';
                            echo $text_area;
                        echo '</div>';
                    }

                // End loop.
                endwhile;

                echo '</div>';

            endif;

        echo '</div>';

    echo '</section>';

}