<?php

function onja_do_section_left_image_right_text() {

    $left_column = get_sub_field( 'left_column' );
    $left_image = $left_column['image'];
    $left_column_bg = $left_column['column_background_color'];

    $right_column = get_sub_field( 'right_column' );
    $right_column_bg = $right_column['column_background_color'];
    $section_heading = $right_column['section_heading'];
    $section_description = $right_column['section_description'];
    $button_text = $right_column['button']['button_text'];
    $button_url = $right_column['button']['button_url']['url'];
    $button_text_color = $right_column['button']['button_text_color'];
    $button_background_color = $right_column['button']['button_background_color'];

    echo '<section class="section-left-image-text-right flex flex-wrap">';

        echo '<div class="w-full order-last lg:order-first lg:w-1/2 xl:w-7/12" style="background-color: ' . $left_column_bg . '">';
            echo '<div class="py-10 px-4 flex items-center md:py-16 md:px-8 xl:pl-16 xl:pr-24">';
                echo wp_get_attachment_image( $left_image, 'full', "", array( "class" => "w-full" ) );
            echo '</div>';
        echo '</div>';

        echo '<div class="flex items-center w-full order-first lg:order-last lg:w-1/2 xl:w-5/12" style="background-color: ' . $right_column_bg . '">';
            echo '<div class="py-12 px-4 md:px-8 xl:p-16">';
                echo '<h3 class="section--heading text-2xl font-extrabold uppercase pt-0 mb-8 border-white border-0 text-white md:pt-4 md:text-3xl md:mb-12 md:border-t">' . $section_heading . '</h3>';
                echo '<div class="section--description text-lg text-white font-normal mb-10 md:text-xl">';
                    echo $section_description;
                    // echo '<p class="mb-5">We upskill the world’s forgotten talent, preparing capable youth for work as software developers.</p>';
                    // echo '<p class="mb-5">Companies then hire our developers, located in Africa, to work remotely in their tech teams.</p>';
                    // echo '<p class="mb-5">Our developers are Front-End (web) specialists with strong CSS, JavaScript and React.js skill. Consider us for any front-end role!</p>';
                echo '</div>';
                echo '<a href="' . $button_url . '" class="inline-block py-2 px-6 bg-white font-bold rounded-full text-lg shadow-lg transition hover:shadow-2xl md:py-3 md:px-8 md:text-xl" style="color: ' . $button_text_color . '; background-color: ' . $button_background_color . '">' . $button_text . '</a>';
            echo '</div>';
        echo '</div>';

    echo '</section>';

}