<?php

function onja_do_section_posts_archive() {

    $background_color = get_sub_field( 'background_color' );
    $vertical_padding = get_sub_field( 'vertical_padding' );

    if ( $vertical_padding == 'sm' ) {
        $vertical_padding = 'py-4 lg:py-6 xl:py-8';
    } else if ( $vertical_padding == 'md' ) {
        $vertical_padding = 'py-6 lg:py-8 xl:py-12';
    } else if ( $vertical_padding == 'lg' ) {
        $vertical_padding = 'py-8 lg:py-12 xl:py-16';
    } else if ( $vertical_padding == 'xl' ) {
        $vertical_padding = 'py-10 lg:py-16 xl:py-20';
    } else if ( $vertical_padding == '2xl' ) {
        $vertical_padding = 'py-12 lg:py-16 xl:py-24';
    } else if ( $vertical_padding == '3xl' ) {
        $vertical_padding = 'py-16 lg:py-20 xl:py-28';
    } else {
        $vertical_padding = 'py-8 lg:py-12 xl:py-16';
    }

    $content_max_width = get_sub_field( 'content_max_width' );

    $section_anchor = get_sub_field( 'section_anchor' );
    $section_anchor_id = get_sub_field( 'section_anchor_id' );

    $section_heading = get_sub_field( 'section_heading' );
    $display_posts_from = get_sub_field( 'display_posts_from' );

    echo '<section id="' . esc_attr( $section_anchor_id ) . '" class="section-text-editor px-4 lg:px-6 xl:px-8 ' . $vertical_padding . '" style="background-color: ' . $background_color . '">';
        echo '<div class="container mx-auto">';
            echo '<div class="mx-auto" style="max-width: ' .  $content_max_width . '">';

                //echo $display_posts_from;
                // echo '<pre>';
                // print_r($display_posts_from);
                // echo '</pre>';

                global $post;

                // arguments, adjust as needed
                $args = array(
                    'post_type'      => 'post',
                    'posts_per_page' => 9,
                    'post_status'    => 'publish',
                    'paged'          => get_query_var( 'paged' )
                );

                /*
                Overwrite $wp_query with our new query.
                The only reason we're doing this is so the pagination functions work,
                since they use $wp_query. If pagination wasn't an issue,
                use: https://gist.github.com/3218106
                */
                global $wp_query;
                $wp_query = new WP_Query( $args );

                if ( have_posts() ) :
                    echo '<div class="grid grid-cols-1 gap-8 md:grid-cols-2 md:gap-6 xl:grid-cols-3 xl:gap-8">';
                    while ( have_posts() ) : the_post();

                        echo '<div class="card_box__col w-full h-auto">';

                            get_template_part( 'templates/partials/archive-post' );

                        echo '</div>';

                    endwhile;
                    echo '</div>';
                    do_action( 'genesis_after_endwhile' );
                endif;

                wp_reset_query();

                // if( $display_posts_from ) {
                //     echo '<div class="grid grid-cols-3 gap-8">';

                //         foreach( $display_posts_from as $post ):
                //         endforeach;

                //     echo '</div>';
                // }

            echo '</div>';
        echo '</div>';
    echo '</section>';
}