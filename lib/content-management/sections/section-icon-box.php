<?php

function onja_do_section_icon_box() {
    echo '<section class="bg-cerulean px-4 py-12 lg:px-6 xl:px-8 xl:py-24">';
        echo '<div class="container mx-auto max-w-screen-xl">';

            $section_heading = get_sub_field('section_heading');

            if ( $section_heading ) {
                echo '<div class="mb-5 xl:mb-12">';
                    echo '<h3 class="text-center font-bold text-4xl leading-tight text-white">' . $section_heading . '</h3>';
                echo '</div>';
            }

            if( have_rows('icon_box') ):

                //echo '<div class="icon-box-wrap grid grid-cols-1 gap-12 py-8 md:grid-cols-2 md:gap-14 lg:grid-cols-3 lg:gap-14 xl:gap-20">';
                echo '<div class="icon-box-wrap flex flex-col py-8 md:flex-row md:flex-wrap md:-mx-6 lg:-mx-8">';

                    // Loop through rows.
                    while( have_rows('icon_box') ) : the_row();

                        // Load sub field value.
                        $icon_title = get_sub_field('icon_title');
                        $icon_image = get_sub_field('icon_image');
                        $icon_description = get_sub_field('icon_description');
                        $status = get_sub_field('status');

                        if ($status) {

                            echo '<div class="icon-box text-center text-white mb-10 md:w-1/2 md:px-8 lg:w-1/3 lg:px-10">';
                                echo '<div class="mb-4"><img class="inline-block w-10 h-auto" src="' . esc_url( $icon_image ) . '"></div>';
                                echo '<h4 class="mb-2 uppercase text-xl font-bold">' . $icon_title . '</h4>';
                                echo '<div class="text-base xl:text-xl">' . $icon_description . '</div>';
                            echo '</div>';

                        }

                    // End loop.
                    endwhile;

                echo '</div>';

            // No value.
            else :
                // Do something...
            endif;


        echo '</div>';
    echo '</section>';
}