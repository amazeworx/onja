<?php

function onja_do_section_model_card() {

    if( have_rows('model_card') ):

        echo '<section class="bg-aliceblue py-8 px-4 md:py-12 lg:py-16 lg:px-6 xl:px-8">';
            echo '<div class="container mx-auto">';

            // Loop through rows.
            while( have_rows('model_card') ) : the_row();

                // Load sub field value.
                $layout = get_sub_field('layout');
                $image = get_sub_field('image');
                $icon_image = get_sub_field('icon_image');
                $icon_text = get_sub_field('icon_text');
                $description = get_sub_field('description');
                // Do something...
                onja_do_social_model_card(array(
                    'layout' => $layout,
                    'image' => $image,
                    'description' => $description,
                    'icon_image' => $icon_image,
                    'icon_text' => $icon_text
                ));

            // End loop.
            endwhile;

            echo '</div>';
        echo '</section>';

    // No value.
    else :
        // Do something...
    endif;

}

function onja_do_social_model_card($args) {

    $order_class = ( $args['layout'] == 'image_right' ) ? 'md:order-last' : '' ;
    $icon_order_class = ( $args['layout'] == 'image_right' ) ? 'xl:order-last' : '' ;
    $border_class = ( $args['layout'] == 'image_right' ) ? 'xl:border-l' : 'xl:border-r' ;
    $text_class = ( $args['layout'] == 'image_right' ) ? 'xl:text-right' : 'xl:text-left' ;

    echo '<div class="bullet_card bullet_card--img_right w-full bg-white flex flex-wrap mb-6 md:mb-10 md:flex-no-wrap">';

        echo '<div class="w-full md:w-1/2 ' . $order_class . '">';
            echo '<img class="object-cover h-64 w-full md:h-96" src="' . $args['image'] . '">';
        echo '</div>';

        echo '<div class="w-full flex flex-wrap items-center content-center pt-4 pb-8 md:py-8 md:w-1/2 xl:flex-no-wrap xl:py-10">';

            echo '<div class="w-full px-4 border-blackcoral text-center mb-4 -mt-20 flex flex-wrap md:flex-no-wrap md:items-center md:mt-0 md:px-6 xl:w-1/3 xl:flex-wrap xl:px-10 ' . $border_class . ' ' . $icon_order_class . '">';
                echo '<div class="w-full mb-2 md:w-auto md:px-0  md:pr-3 md:mb-0 xl:mb-2 xl:px-3 xl:w-full "><img class="w-28 mx-auto md:w-24 md:ml-0 xl:mx-auto" src="' . $args['icon_image'] . '"></div>';
                echo '<div class="w-full text-cerulean text-xl leading-snug uppercase font-bold text-center md:text-left xl:text-center md:w-auto xl:w-full">' . $args['icon_text'] . '</div>';
            echo '</div>';

            echo '<div class="w-full px-4 md:px-6 xl:w-2/3 xl:px-10">';
                echo '<div class="font-light ' . $text_class .'">';
                    echo $args['description'];
                echo '</div>';
            echo '</div>';

        echo '</div>';

    echo '</div>';

}