<?php

    // Components Functions
    //include('components/section-settings.php');

    // Content Functions
    include('sections/section-hero.php');
    include('sections/section-left-image-right-text.php');
    include('sections/section-model-card.php');
    include('sections/section-text-editor.php');
    include('sections/section-icon-card.php');
    include('sections/section-icon-box.php');
    include('sections/section-supporters.php');
    include('sections/section-posts-archive.php');

    // Do Content Management Function
    function onja_do_content_management() {

        $flex_content = 'content_blocks';
        $post_id = get_the_ID();

        // Check value exists.
        if (have_rows($flex_content, $post_id)):

            // Loop through rows.
            while (have_rows($flex_content, $post_id)) : the_row();

                $selected_layout = get_row_layout();

                switch ($selected_layout) {

                    /* -- format example -- /
                    case "section_name":
                        echo get_layout_function();
                        break;
                    /* -- example end -- */

                    case "section_hero":
                        onja_do_section_hero();
                        break;

                    case "section_left_image_right_text":
                        onja_do_section_left_image_right_text();
                        break;

                    case "section_model_card":
                        onja_do_section_model_card();
                        break;

                    case "section_text_editor":
                        onja_do_section_text_editor();
                        break;

                    case "section_icon_card":
                        onja_do_section_icon_card();
                        break;

                    case "section_icon_box":
                        onja_do_section_icon_box();
                        break;

                    case "section_supporters":
                        onja_do_section_supporters();
                        break;

                    case "section_posts_archive":
                        onja_do_section_posts_archive();
                        break;

                    default:
                        break;
                }

            // End loop.
            endwhile;

        // No value.
        else :
            // Invalid ACF form ID
        endif;


    }