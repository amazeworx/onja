<?php

$subheadline = get_sub_field( 'subheadline' );
$subheadline_title = $subheadline['subheadline_title'];
$subheadline_color = $subheadline['subheadline_color'];
$subheadline_size = $subheadline['subheadline_size'];
$subheadline_alignment = $subheadline['subheadline_alignment'];
if ( $subheadline_size == 'sm' ) {
    $subheadline_size = 'text-sm';
} else if ( $subheadline_size == 'md' ) {
    $subheadline_size = 'text-md';
} else if ( $subheadline_size == 'lg' ) {
    $subheadline_size = 'text-lg';
} else if ( $subheadline_size == 'xl' ) {
    $subheadline_size = 'text-lg md:text-xl';
} else if ( $subheadline_size == '2xl' ) {
    $subheadline_size = 'text-xl md:text-2xl';
} else if ( $subheadline_size == '3xl' ) {
    $subheadline_size = 'text-2xl md:text-3xl';
} else if ( $subheadline_size == '4xl' ) {
    $subheadline_size = 'text-2xl md:text-4xl';
} else if ( $subheadline_size == '5xl' ) {
    $subheadline_size = 'text-3xl md:text-5xl';
} else {
    $subheadline_size = 'text-3xl md:text-5xl';
}