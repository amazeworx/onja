<?php

$background_color = get_sub_field( 'background_color' );
$background_image = get_sub_field( 'background_image' );
$background_overlay = get_sub_field( 'background_overlay' );
$vertical_padding = get_sub_field( 'vertical_padding' );
if ( $vertical_padding == 'sm' ) {
    $vertical_padding = 'py-4 lg:py-6 xl:py-8';
} else if ( $vertical_padding == 'md' ) {
    $vertical_padding = 'py-6 lg:py-8 xl:py-12';
} else if ( $vertical_padding == 'lg' ) {
    $vertical_padding = 'py-8 md:py-12 lg:py-12 xl:py-16';
} else if ( $vertical_padding == 'xl' ) {
    $vertical_padding = 'py-10 lg:py-16 xl:py-20';
} else if ( $vertical_padding == '2xl' ) {
    $vertical_padding = 'py-12 lg:py-16 xl:py-24';
} else if ( $vertical_padding == '3xl' ) {
    $vertical_padding = 'py-12 md:py-16 lg:py-20 xl:py-28';
} else if ( $vertical_padding == '4xl' ) {
    $vertical_padding = 'py-12 md:py-20 lg:py-24 xl:py-32';
} else {
    $vertical_padding = 'py-8 lg:py-12 xl:py-16';
}
$content_max_width = get_sub_field( 'content_max_width' );
$section_anchor = get_sub_field( 'section_anchor' );
$section_anchor_id = get_sub_field( 'section_anchor_id' );