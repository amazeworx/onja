<?php

$headline = get_sub_field( 'headline' );
$headline_title = $headline['headline_title'];
$headline_title_color = $headline['headline_title_color'];
$headline_title_size = $headline['headline_title_size'];
$headline_alignment = $headline['headline_alignment'];
if ( $headline_title_size == 'sm' ) {
    $headline_title_size = 'text-sm';
} else if ( $headline_title_size == 'md' ) {
    $headline_title_size = 'text-md';
} else if ( $headline_title_size == 'lg' ) {
    $headline_title_size = 'text-lg';
} else if ( $headline_title_size == 'xl' ) {
    $headline_title_size = 'text-lg md:text-xl';
} else if ( $headline_title_size == '2xl' ) {
    $headline_title_size = 'text-xl md:text-2xl';
} else if ( $headline_title_size == '3xl' ) {
    $headline_title_size = 'text-2xl md:text-3xl';
} else if ( $headline_title_size == '4xl' ) {
    $headline_title_size = 'text-2xl md:text-4xl';
} else if ( $headline_title_size == '5xl' ) {
    $headline_title_size = 'text-3xl md:text-5xl';
} else {
    $headline_title_size = 'text-3xl md:text-5xl';
}