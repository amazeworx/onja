<?php

function onja_do_recent_posts_slider( $args ) {

    $args = shortcode_atts(
        [
            'post_type'      => 'post',
            'posts_per_page' => get_option( 'posts_per_page' ),
            'add_view_more'  => false,
            'slider' => false,
        ],
        $args,
        'onja_recent_posts'
    );

    $posts = get_posts( $args );

    if ( empty( $posts ) ) {
        return;
    }

    global $post;

    ob_start();

    $is_slider = $args['slider'];

    if ($is_slider == true) {
        echo '<div class="relative -mx-4 md:mx-0">';
            echo '<div class="recent-posts-swiper swiper-container">';
                echo '<div class="swiper-wrapper flex">';
    } else {
        echo '<div class="grid grid-cols-3 gap-8">';
    }

    foreach ( $posts as $post ) {
        setup_postdata( $post );
        echo '<div class="swiper-slide h-auto px-4 w-full md:w-1/2 xl:w-1/3">';

            get_template_part( 'templates/partials/archive-post' );

        echo '</div>';
    }
    wp_reset_postdata();

    if ($is_slider) {
                echo '</div>';
            echo '</div>';
            echo '<div class="swiper-pagination"></div>';
            echo '<div class="swiper-button-next"></div>';
            echo '<div class="swiper-button-prev"></div>';
        echo '</div>';
    } else {
        echo '</div>';
    }

    // add archive link
    $is_add_view_more = $args['add_view_more'];
    if ($is_add_view_more == true) {
        echo '<div class="more-button text-center py-3 mt-16 md:mt-16">';
            echo '<a href="/blog/" class="inline-block uppercase font-semibold text-center text-lg bg-cerulean text-white rounded-full px-6 py-2 transition-shadow hover:bg-crayola hover:shadow-md hover:text-white md:py-3 md:px-8">All Updates</a>';
        echo '</p>';
    }

    return ob_get_clean();

}
add_shortcode( 'onja_recent_posts', 'onja_do_recent_posts_slider' );