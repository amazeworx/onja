<?php
/**
 * This file adds popups functionality to Onja Pro theme
 *
 * @package Onja Pro
 * @author OksanaRomaniv <okskozub@gmail.com>
 */

class ON_Popups {
	public function __construct() {
		// add subscription popup to shortcode
		add_shortcode( 'onja_subscribe_popup', [ 'ON_Popups', 'show_onja_subscribe_popup' ] );

		// add global donation popup
		add_action( 'genesis_after', [ 'ON_Popups', 'show_onja_donate_popup' ] );
	}

	/**
	 * Add global donation popup
	 *
	 * @return void
	 */
	public static function show_onja_donate_popup() {
		echo '<div style="display: none;" id="donationPopup" class="onja_popup onja_popup--donation">';
			echo '<div class="onjaDonation text-center"></div>';
		echo '</div>';
	}

	/**
	 * Render Subscribe Popup
	 *
	 * @return void
	 */
	public static function show_onja_subscribe_popup() {
		$newsletter_popup = get_field( 'newsletter_popup', 'options' );

		if ( empty( $newsletter_popup ) ) {
			return;
		}

		$out = '';

		$out .= '<div class="onja_popup onja_popup--newsletter" id="onja_popup--newsletter">';
		if ( isset( $newsletter_popup['featured_image'] ) ) {
			$out     .= '<div class="onja_popup__featured relative">';
				$out .= wp_get_attachment_image( $newsletter_popup['featured_image'], 'full' );
				$out .= '<h3 class="onja_popup__title absolute w-full bottom-0 text-white text-center font-bold py-5 text-4xl md:py-8 md:text-5xl">' . esc_html( $newsletter_popup['title'] ) . '</h3>';
			$out     .= '</div>';
		}

		if ( isset( $newsletter_popup['popup_content'] ) && ! empty( $newsletter_popup['popup_content'] ) ) {
			$out .= '<div class="onja_popup__content onja_popup_ninjaform">';
			$out .= $newsletter_popup['popup_content'];
			$out .= '</div>';
		}

		$out .= '</div>';

		return $out;
	}
}
new ON_Popups();
