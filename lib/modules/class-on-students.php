<?php
/**
 * This class add student CTP to the Onja Pro theme
 *
 * @package Onja Pro
 * @author OksanaRomaniv <okskozub@gmail.com>
 */

class ON_Students {
	public function __construct() {
		// Create CPT
		add_action( 'init', [ 'ON_Students', 'create_student_cpt' ], 0 );
		add_action( 'init', [ 'ON_Students', 'add_student_tags' ], 0 );

		// Create Featured Students Slider
		add_shortcode( 'onja_featured_students', [ 'ON_Students', 'show_featured_slider' ] );

		// Customize single student page
		add_action( 'genesis_meta', [ 'ON_Students', 'customize_single_student' ] );

	}

	/**
	 * Customize single student page
	 *
	 * @return void
	 */
	public static function customize_single_student() {
		if ( ! is_singular( 'student' ) ) {
			return;
		}

		// Filter the splash title
		// add_filter(
		// 	'onja_splash_title',
		// 	function( $title ) {
		// 		$archive_settings = get_option( 'genesis-cpt-archive-settings-student' );

		// 		$title = ( isset( $archive_settings['headline'] ) && ! empty( $archive_settings['headline'] ) ) ? $archive_settings['headline'] : __( 'Our Students', 'onja-pro' );

		// 		return $title;
		// 	}
		// );

		// Filter the splash tag
		// add_filter(
		// 	'onja_splash_title_tag',
		// 	function() {
		// 		return 'h2';
		// 	}
		// );

		// Add intro line to splash content
		// add_filter(
		// 	'onja_splash_content',
		// 	function( $content ) {
		// 		$archive_settings = get_option( 'genesis-cpt-archive-settings-student' );

		// 		$content = ( isset( $archive_settings['intro_text'] ) && ! empty( $archive_settings['intro_text'] ) ) ? nl2br( $archive_settings['intro_text'] ) : '';

		// 		return $content;
		// 	}
		// );

		// Center the splash content
		// add_filter(
		// 	'onja_splash_content_position',
		// 	function() {
		// 		return 'center';
		// 	}
		// );

		// Enable Splash Overlay
		//add_filter( 'onja_splash_with_overlay', '__return_true' );

		// Filter the featured image
		// add_filter(
		// 	'onja_splash_image',
		// 	function( $background ) {
		// 		$background = ( get_field( 'default_featured_image_student', 'options' ) ) ?: $background;

		// 		return $background;
		// 	}
        // );

		//* Remove the entry header markup (requires HTML5 theme support)
		remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
		remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );
		remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

		//* Remove the entry footer markup (requires HTML5 theme support)
		remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_open', 5 );
		remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_close', 15 );
		remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

		add_action( 'genesis_entry_content', [ 'ON_Students', 'onja_student_header' ], 8 );

		// * Add after entry area
		add_action( 'genesis_after_entry', [ 'ON_Students', 'onja_after_student' ], 9 );
	}

	/**
	 * Onja After Entry Area for Student CPT
	 *
	 * @return void
	 */
	public static function onja_after_student() {
		if ( ! is_singular( 'student' ) ) {
			return;
		}

		echo '<div class="studentFooter mt-4 mb-16 px-4 md:px-0 md:-mx-4 md:mt-8 lg:mt-12 lg:mb-24">';
		echo do_shortcode( '[onja_featured_students add_view_more="false"]' );
		echo '</div>';
	}

	/**
	 * Output the header block
	 *
	 * @return void
	 */
	public static function onja_student_header() {
		if ( ! is_singular( 'student' ) ) {
			return;
		}

        echo '<div class="studentHeader relative pt-4 pb-0 flex flex-row flex-wrap md:pt-8 lg:flex-no-wrap lg:mb-8 lg:pt-12" data-donation-name="' . get_the_title() . '" data-donation-id="' . get_the_ID() . '" data-donation-status="' . get_post_meta( get_the_ID(), 'onja_donation_status', true ) . '">';
            echo '<div class="studentHeader__image w-full overflow-hidden rounded-2xl mb-5 lg:w-1/2 lg:mb-0 lg:pr-8">';
                the_post_thumbnail( 'student-featured' );
            echo '</div>';
            echo '<div class="studentHeader__info w-full flex flex-row flex-no-wrap items-end justify-between lg:w-1/2 lg:pl-0">';
                echo '<div class="w-full border-aliceblue lg:pt-6 lg:border-t">';
                    genesis_do_post_title();
                echo '</div>';
                ON_Donation_Module::show_student_donation_info();
                ON_Donation_Module::show_student_donate();
                // echo '<div class="post-share py-4 flex flex-row flex-wrap items-center justify-center opacity-50">';
                //     echo '<span>Share it on</span>' . do_shortcode( '[addtoany]' );
                // echo '</div>';
            echo '</div>';
        echo '</div>';
	}

	/**
	 * Show Students Featured Slider
	 *
	 * @return void
	 */
	public static function show_featured_slider( $atts ) {

		// get students with tag 'featured'
		$atts = shortcode_atts(
			[
				'add_view_more'  => true,
				'posts_per_page' => '-1',
			],
			$atts,
			'onja_featured_students'
		);

		$args = [
			'post_type'      => 'student',
			'posts_per_page' => $atts['posts_per_page'],
			'tax_query'      => [
				[
					'taxonomy' => 'student_tag',
					'field'    => 'slug',
					'terms'    => 'featured',
				],
			],
			'order'          => 'ASC',
		];

		if ( is_singular( 'student' ) ) {
			$args['post__not_in'] = [ get_the_ID() ];
		}

		$students = get_posts( $args );

		// If nothing found - return
		if ( empty( $students ) ) {
			return;
		}

		ob_start();
		global $post;

		// build loop
        //echo '<div class="featured-students content-slider">';
        echo '<div class="relative -mx-4 md:mx-0">';
            echo '<div class="recent-posts-swiper swiper-container">';
                echo '<div class="swiper-wrapper flex">';

		foreach ( $students as $post ) {
			setup_postdata( $post );
            //echo '<div class="card_box__col">';
            echo '<div class="swiper-slide h-auto px-4 w-full md:w-1/2 xl:w-1/3">';
				get_template_part( 'templates/partials/archive-post' );
			echo '</div>';
		}
        wp_reset_postdata();

                echo '</div>';
            echo '</div>';
            echo '<div class="swiper-pagination"></div>';
            echo '<div class="swiper-button-next"></div>';
            echo '<div class="swiper-button-prev"></div>';
        echo '</div>';

        //echo '</div>';

		// add archive link
		if ( true === filter_var( $atts['add_view_more'], FILTER_VALIDATE_BOOLEAN ) ) {
			echo '<p class="text-center py-3 more-button"><a href="' . esc_url( get_post_type_archive_link( 'student' ) ) . '" class="btn btn--blue btn--wide">All Students</a></p>';
		}

		return ob_get_clean();
	}

	/**
	 * Create Student Post Type
	 *
	 * @return void
	 */
	public static function create_student_cpt() {
		$labels = array(
			'name'                  => _x( 'Students', 'Post Type General Name', 'onja-pro' ),
			'singular_name'         => _x( 'Student', 'Post Type Singular Name', 'onja-pro' ),
			'menu_name'             => _x( 'Students', 'Admin Menu text', 'onja-pro' ),
			'name_admin_bar'        => _x( 'Student', 'Add New on Toolbar', 'onja-pro' ),
			'archives'              => __( 'Student Archives', 'onja-pro' ),
			'attributes'            => __( 'Student Attributes', 'onja-pro' ),
			'parent_item_colon'     => __( 'Parent Student:', 'onja-pro' ),
			'all_items'             => __( 'All Students', 'onja-pro' ),
			'add_new_item'          => __( 'Add New Student', 'onja-pro' ),
			'add_new'               => __( 'Add New', 'onja-pro' ),
			'new_item'              => __( 'New Student', 'onja-pro' ),
			'edit_item'             => __( 'Edit Student', 'onja-pro' ),
			'update_item'           => __( 'Update Student', 'onja-pro' ),
			'view_item'             => __( 'View Student', 'onja-pro' ),
			'view_items'            => __( 'View Students', 'onja-pro' ),
			'search_items'          => __( 'Search Student', 'onja-pro' ),
			'not_found'             => __( 'Not found', 'onja-pro' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'onja-pro' ),
			'featured_image'        => __( 'Featured Image', 'onja-pro' ),
			'set_featured_image'    => __( 'Set featured image', 'onja-pro' ),
			'remove_featured_image' => __( 'Remove featured image', 'onja-pro' ),
			'use_featured_image'    => __( 'Use as featured image', 'onja-pro' ),
			'insert_into_item'      => __( 'Insert into Student', 'onja-pro' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Student', 'onja-pro' ),
			'items_list'            => __( 'Students list', 'onja-pro' ),
			'items_list_navigation' => __( 'Students list navigation', 'onja-pro' ),
			'filter_items_list'     => __( 'Filter Students list', 'onja-pro' ),
		);
		$args   = array(
			'label'               => __( 'Student', 'onja-pro' ),
			'description'         => __( 'Onja Students', 'onja-pro' ),
			'labels'              => $labels,
			'menu_icon'           => 'dashicons-welcome-learn-more',
			'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'page-attributes', 'custom-fields', 'genesis-seo', 'genesis-cpt-archives-settings', 'genesis-scripts', 'author' ),
			'taxonomies'          => array(),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => 'students',
			'hierarchical'        => false,
			'exclude_from_search' => false,
			'show_in_rest'        => true,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
		);
		register_post_type( 'student', $args );

	}

	// Register Student Tag
	public static function add_student_tags() {

		$labels = array(
			'name'                       => _x( 'Tags', 'Taxonomy General Name', 'onja-pro' ),
			'singular_name'              => _x( 'Tag', 'Taxonomy Singular Name', 'onja-pro' ),
			'menu_name'                  => __( 'Tags', 'onja-pro' ),
			'all_items'                  => __( 'All Tags', 'onja-pro' ),
			'parent_item'                => __( 'Parent Tag', 'onja-pro' ),
			'parent_item_colon'          => __( 'Parent Tag:', 'onja-pro' ),
			'new_item_name'              => __( 'New Tag Name', 'onja-pro' ),
			'add_new_item'               => __( 'Add New Tag', 'onja-pro' ),
			'edit_item'                  => __( 'Edit Tag', 'onja-pro' ),
			'update_item'                => __( 'Update Tag', 'onja-pro' ),
			'view_item'                  => __( 'View Tag', 'onja-pro' ),
			'separate_items_with_commas' => __( 'Separate Tags with commas', 'onja-pro' ),
			'add_or_remove_items'        => __( 'Add or remove Tags', 'onja-pro' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'onja-pro' ),
			'popular_items'              => __( 'Popular Tags', 'onja-pro' ),
			'search_items'               => __( 'Search Tags', 'onja-pro' ),
			'not_found'                  => __( 'Not Found', 'onja-pro' ),
			'no_terms'                   => __( 'No Tags', 'onja-pro' ),
			'items_list'                 => __( 'Tags list', 'onja-pro' ),
			'items_list_navigation'      => __( 'Tags list navigation', 'onja-pro' ),
		);
		$args   = array(
			'labels'            => $labels,
			'hierarchical'      => false,
			'public'            => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'     => true,
			'show_in_rest'      => true,
		);
		register_taxonomy( 'student_tag', array( 'student' ), $args );

	}
}
new ON_Students();
