<?php
/**
 * This file adds Maijet integration to Onja Pro theme
 *
 * @package Onja Pro
 * @author OksanaRomaniv <okskozub@gmail.com>
 */

require_once( get_stylesheet_directory() . '/vendor/autoload.php' );
use \Mailjet\Resources;

class ON_Mailjet {
	/**
	 * Mailjet Integration Config
	 */
	var $config = [];

	/**
	 * On Class INIT
	 */
	public function __construct() {
		$this->config = get_field( 'mailjet_config', 'options' );
	}

	/**
	 * Send One-Time donation confirm
	 *
	 * @return void
	 */
	public static function send_mailjet_transactional_email_payment( $args, $template_id ) {

		// Extract Variables
		$donor_name            = ( isset( $args['donor_name'] ) ) ? $args['donor_name'] : '';
		$donor_address         = ( isset( $args['donor_address'] ) ) ? $args['donor_address'] : '';
		$donor_country         = ( isset( $args['donor_country'] ) ) ? $args['donor_country'] : '';
		$donor_city            = ( isset( $args['donor_city'] ) ) ? $args['donor_city'] : '';
		$donor_email           = ( isset( $args['receipt_email'] ) ) ? $args['receipt_email'] : '';
		$receipt_number        = ( isset( $args['receipt_number'] ) ) ? $args['receipt_number'] : '';
		$donation_date         = ( isset( $args['donation_date'] ) ) ? date( 'M d, Y', $args['donation_date'] ) : '';
		$donation_amount       = ( isset( $args['donation_amount'] ) ) ? number_format( $args['donation_amount'] / 100 ) : '';
		$donation_currency     = ( isset( $args['donation_currency'] ) ) ? strtoupper( $args['donation_currency'] ) : '';
		$subscription_end_date = ( isset( $args['subscription_end_date'] ) ) ? date( 'M d, Y', $args['subscription_end_date'] ) : '';

		// Get Mailjet connection config
		$config = get_field( 'mailjet_config', 'options' );

		// Init Mailjet Client
		$mj = new \Mailjet\Client( $config['api_key'], $config['api_secret'], true, [ 'version' => 'v3.1' ] );

		// Define Request Body
		$body = [
			'Messages' => [
				[
					'To'               => [
						[
							'Email' => $donor_email,
							'Name'  => $donor_name,
						],
					],
					'TemplateID'       => (int) $template_id,
					'TemplateLanguage' => true,
					'Variables'        => json_decode(
						'{
							"donor_name": "' . $donor_name . '",
							"donor_address": "' . $donor_address . '",
							"donor_country": "' . $donor_country . '",
							"donor_city": "' . $donor_city . '",
							"receipt_number": "' . $receipt_number . '",
							"donation_date": "' . $donation_date . '",
							"donation_amount": "' . $donation_amount . '",
							"donation_currency": "' . $donation_currency . '",
							"subscription_end_date":"' . $subscription_end_date . '"
						}',
						true
					),
				],
			],
		];

		if ( isset( $config['bcc_email'] ) ) {
			$body['Messages'][0]['Bcc'][0]['Email'] = sanitize_email( $config['bcc_email'] );
		}

		// Send Email
		$response = $mj->post( Resources::$Email, [ 'body' => $body ] );
    }

	/**
	 * Send Email Subscription Confirmation
	 *
	 * @return void
	 */
	public static function send_mailjet_subscription_confirmation( $args, $template_id ) {

		// Extract Variables
		$first_name = ( isset( $args['first_name'] ) ) ? $args['first_name'] : '';
		$email = ( isset( $args['email'] ) ) ? $args['email'] : '';

		// Get Mailjet connection config
		$config = get_field( 'mailjet_config', 'options' );

		// Init Mailjet Client
		$mj = new \Mailjet\Client( $config['api_key'], $config['api_secret'], true, [ 'version' => 'v3.1' ] );

		// Define Request Body
		$body = [
			'Messages' => [
				[
					'To'               => [
						[
							'Email' => $email,
							'Name'  => $first_name,
						],
					],
					'TemplateID'       => (int) $template_id,
					'TemplateLanguage' => true,
					'Variables'        => json_decode(
						'{
							"first_name": "' . $first_name . '"
						}',
						true
					),
				],
			],
		];

		// if ( isset( $config['bcc_email'] ) ) {
		// 	$body['Messages'][0]['Bcc'][0]['Email'] = sanitize_email( $config['bcc_email'] );
		// }

		// Send Email
		$response = $mj->post( Resources::$Email, [ 'body' => $body ] );
    }

	/**
	 * Send Get In Touch Confirmation
	 *
	 * @return void
	 */
	public static function send_mailjet_getintouch_confirmation( $args, $template_id ) {

		// Extract Variables
		$full_name = ( isset( $args['full_name'] ) ) ? $args['full_name'] : '';
		$email = ( isset( $args['email'] ) ) ? $args['email'] : '';

		// Get Mailjet connection config
		$config = get_field( 'mailjet_config', 'options' );

		// Init Mailjet Client
		$mj = new \Mailjet\Client( $config['api_key'], $config['api_secret'], true, [ 'version' => 'v3.1' ] );

		// Define Request Body
		$body = [
			'Messages' => [
				[
					'To'               => [
						[
							'Email' => $email,
							'Name'  => $full_name,
						],
					],
					'TemplateID'       => (int) $template_id,
					'TemplateLanguage' => true,
					'Variables'        => json_decode(
						'{
							"full_name": "' . $full_name . '"
						}',
						true
					),
				],
			],
		];

		// if ( isset( $config['bcc_email'] ) ) {
		// 	$body['Messages'][0]['Bcc'][0]['Email'] = sanitize_email( $config['bcc_email'] );
		// }

		// Send Email
		$response = $mj->post( Resources::$Email, [ 'body' => $body ] );
    }

}
new ON_Mailjet();
