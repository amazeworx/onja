<?php
/**
 * This class manages the donation module for Onja Pro
 *
 * @package Onja Pro\Modules
 * @author OksanaRomaniv <okskozub@gmail.com>
 */

// Require Vendor files
require_once( get_stylesheet_directory() . '/vendor/autoload.php' );

class ON_Donation_Module {

	/**
	 * Donation Goal for the whole period
	 *
	 * @var number
	 */
	public static $donation_goal;

	/**
	 * Donation Goal per Month
	 *
	 * @var number
	 */
	public static $monthly_goal;

	/**
	 * Donation Period Duration (in months)
	 *
	 * @var number
	 */
	public static $donation_period;

	public function __construct() {

        // Set Stripe API
        $stripe_live_mode = get_field( 'testing_mode__live_mode', 'options' );
        if ( $stripe_live_mode ) {
            $key = get_field( 'stripe_api_secret', 'options' );
        } else {
            $key = get_field( 'stripe_api_secret_test', 'options' );
        }
		\Stripe\Stripe::setApiKey( $key );

		// Set donation goal
		$donation_module_settings = get_field( 'donation_module_settings', 'options' );
		self::$donation_period    = $donation_module_settings['donation_period'];
		self::$monthly_goal       = $donation_module_settings['monthly_funding_goal'];
		self::$donation_goal      = self::$donation_period * self::$monthly_goal;

		add_shortcode( 'onja_donation_module', [ 'ON_Donation_Module', 'show_donation_module' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_donation_assets' ] );

		// add ajax functions
		// -> make donation
		add_action( 'wp_ajax_onja_create_charge', 'ON_Donation_Module::onja_create_charge' );
		add_action( 'wp_ajax_nopriv_onja_create_charge', 'ON_Donation_Module::onja_create_charge' );
		// -> reset donation status
		add_action( 'wp_ajax_onja_reset_donation', 'ON_Donation_Module::onja_reset_donation' );
		add_action( 'wp_ajax_nopriv_onja_reset_donation', 'ON_Donation_Module::onja_reset_donation' );
		// -> delete donations
		add_action( 'wp_ajax_onja_delete_donation', 'ON_Donation_Module::onja_delete_donation' );
		add_action( 'wp_ajax_nopriv_onja_delete_donation', 'ON_Donation_Module::onja_delete_donation' );

		// -> receive the recurring monthly donation email
		add_action( 'wp_ajax_onja_stripe_webhooks', 'ON_Donation_Module::onja_stripe_webhooks' );
		add_action( 'wp_ajax_nopriv_onja_stripe_webhooks', 'ON_Donation_Module::onja_stripe_webhooks' );

		// -> add donations history to student profile page for admins
		add_action( 'genesis_before_content', 'ON_Donation_Module::show_admin_donation_history' );
	}

	public static function show_admin_donation_history() {
		if ( ! is_singular( 'student' ) || ! is_user_logged_in() || ! current_user_can( 'edit_pages' ) ) {
			return false;
		}

		wp_enqueue_style( 'tabulator_styles', get_stylesheet_directory_uri() . '/assets/dist/css/tabulator_materialize.min.css', [], '4.5.1', 'all' );

		$donations = get_post_meta( get_the_ID(), 'onja_donation_info', true ) ?: '[]';

		echo '<div id="studentDonations" class="mb-16 hidden">
		<h2 class="mb-4">Donations:</h2>
		<div class="tableData" data-donations="' . esc_attr( $donations ) . '">Loading Donations History...</div>
		</div>';

	}

	/**
	 * Receive the Stripe Invoice Billing Event
	 */
	public static function onja_stripe_webhooks() {

		$payload         = @file_get_contents( 'php://input' );
        $stripe_live_mode = get_field( 'testing_mode__live_mode', 'options' );
        if ( $stripe_live_mode ) {
            $endpoint_secret = get_field( 'stripe_monthly_endpoint_secret', 'options' );
        } else {
            $endpoint_secret = get_field( 'stripe_monthly_endpoint_secret_test', 'options' );
        }
		$sig_header      = $_SERVER['HTTP_STRIPE_SIGNATURE'];
		$event           = null;

		try {
			$event = \Stripe\Webhook::constructEvent(
				$payload,
				$sig_header,
				$endpoint_secret
			);
		} catch ( \UnexpectedValueException $e ) {
			// Invalid payload
			http_response_code( 400 );
			wp_die();
		} catch ( \Stripe\Error\SignatureVerification $e ) {
			// Invalid signature
			http_response_code( 400 );
			wp_die();
		}

		// * Handle Events
		switch ( $event->type ) {
			case 'invoice.payment_succeeded':
				$invoice_object = $event->data->object; // contains a StripePaymentIntent
				self::handle_invoice_payment_succeeded( $invoice_object );
				break;
			default:
				// Unexpected event type
				http_response_code( 400 );
				wp_die();
		}

		http_response_code( 200 );
		wp_die();
	}

	/**
	 * Handle Invoice Payment Success
	 *
	 * @param object $invoice_object
	 * @return void
	 */
	private static function handle_invoice_payment_succeeded( $invoice ) {
		// bail if nothing passed to function
		if ( ! $invoice ) {
			return;
		}

		// check if needed payment reason (we exclude here the invoice sent when starting subscription)
		if ( ! isset( $invoice->subscription ) || 'subscription_cycle' !== $invoice->billing_reason ) {
			return;
		}

		// * Process subscription
		// -> generate receipt number
		$receipt_number = ON_Donation_Module::generate_invoice_num();
		// -> get subscription object
		$sub_id       = $invoice->subscription;
		$subscription = \Stripe\Subscription::retrieve( $sub_id );
		// -> check if subscription has end date
		$subscription_end_date = ( isset( $subscription->cancel_at ) ) ? $subscription->cancel_at : '';
		// -> update subscription meta with last receipt number
		\Stripe\Subscription::update(
			$sub_id,
			[
				'metadata' => [
					'latest_receipt_num' => $receipt_number,
				],
			]
		);

		// * Send Confirmational Email
		$config = get_field( 'mailjet_config', 'options' );
		// bail if no Mailjet configuration or if template not connected
		if ( ! $config || ! isset( $config['monthly_recurring_id'] ) || empty( $config['monthly_recurring_id'] ) ) {
			return;
		}
		ON_Mailjet::send_mailjet_transactional_email_payment(
			[
				'donor_name'            => $invoice->customer_name,
				'donor_address'         => $invoice->customer_address->line1,
				'donor_country'         => $invoice->customer_address->country,
				'donor_city'            => $invoice->customer_address->city,
				'receipt_email'         => $invoice->customer_email,
				'receipt_number'        => $receipt_number,
				'donation_date'         => time(),
				'donation_amount'       => $invoice->amount_paid,
				'donation_currency'     => $invoice->currency,
				'subscription_end_date' => $subscription_end_date,
			],
			$config['monthly_recurring_id']
		);
	}

	// Process admin call to reset donation
	public static function onja_reset_donation() {
		check_ajax_referer( 'donation', 'nonce' );

		// Get Request Payload
		$student_id = ( isset( $_POST['studentID'] ) && ! empty( $_POST['studentID'] ) ) ? sanitize_text_field( $_POST['studentID'] ) : '';

		if ( empty( $student_id ) ) {
			wp_send_json_error( 'No student ID specified' );
		}

		// -> get donation info
		$donation_info = json_decode( get_post_meta( $student_id, 'onja_donation_info', true ), true );
		// -> parse array and remove one marked for deletion
		foreach ( $donation_info as $index => $donation ) {
			if ( isset( $donation['deleteThis'] ) && true === $donation['deleteThis'] ) {
				unset( $donation_info[ $index ] );
			}
		}
		// -> save back filtered array
		update_post_meta( $student_id, 'onja_donation_info', json_encode( $donation_info, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE ) );
		// -> calculate donation sum
		$sum = array_reduce(
			$donation_info,
			function ( $prev_sum, $donation ) {
				$amount_to_add = ( 'one_time' === $donation['type'] ) ? $donation['amount'] : $donation['amount'] * self::$donation_period;
				return $prev_sum + $amount_to_add;
			},
			0
		);
		update_post_meta( $student_id, 'onja_donation_sum', $sum );
		// calculate donation status
		$status = ( $sum >= self::$donation_goal ) ? true : false;
		update_post_meta( $student_id, 'onja_donation_status', $status );

		wp_send_json_success();
	}

	// Process admin call to delete donation
	public static function onja_delete_donation() {
		check_ajax_referer( 'donation', 'nonce' );

		// Get Request Payload
		$student_id = ( isset( $_POST['studentID'] ) && ! empty( $_POST['studentID'] ) ) ? sanitize_text_field( $_POST['studentID'] ) : '';

		if ( empty( $student_id ) ) {
			wp_send_json_error( 'No student ID specified' );
		}

		delete_post_meta( $student_id, 'onja_donation_info' );
		delete_post_meta( $student_id, 'onja_donation_status' );
		delete_post_meta( $student_id, 'onja_donation_sum' );

		wp_send_json_success();
	}

	/**
	 * Generate Receipt/Invoice Number
	 *
	 * @return void
	 */
	public static function generate_invoice_num() {

		$id = substr( hexdec( uniqid() ), 0, 8 );
		$id = substr_replace( $id, '-', 4, 0 );

		return $id;
	}

	// Process Stripe Token data from front-end
	public static function onja_create_charge() {
		check_ajax_referer( 'donation', 'nonce' );

		// Get Request Payload
		$payload = json_decode( file_get_contents( 'php://input' ), true );
		$error   = [];
		$success = false;

		if ( ! isset( $payload['admin'] ) || false === $payload['admin'] ) {
			try {

				// ! VAR 1 : process single payment
				if ( false === $payload['paymentDetails']['monthly'] ) {
					$stripe_end_object = self::process_one_time_payment( $payload );
				}

				// ! VAR 2 : process recurring payment
				if ( true === $payload['paymentDetails']['monthly'] ) {
					$stripe_end_object = self::process_recurring_payment( $payload );
				}

				// * processed payment successfully
				$success = true;

			} catch ( \Stripe\Error\Card $e ) {
				// Since it's a decline, \Stripe\Error\Card will be caught
				$error['code']    = $e->getDeclineCode();
				$error['message'] = $e->getMessage();
			} catch ( \Stripe\Error\RateLimit $e ) {
				// Too many requests made to the API too quickly
				$error['message'] = $e->getMessage();
			} catch ( \Stripe\Error\InvalidRequest $e ) {
				// Invalid parameters were supplied to Stripe's API
				$error['message'] = $e->getMessage();
			} catch ( \Stripe\Error\Authentication $e ) {
				// Authentication with Stripe's API failed
				// (maybe you changed API keys recently)
				$error['message'] = $e->getMessage();
			} catch ( \Stripe\Error\ApiConnection $e ) {
				// Network communication with Stripe failed
				$error['message'] = $e->getMessage();
			} catch ( \Stripe\Error\Base $e ) {
				// Display a very generic error to the user, and maybe send
				// yourself an email
				$error['message'] = $e->getMessage();
			} catch ( Exception $e ) {
				$error['message'] = $e->getMessage();
				// Something else happened, completely unrelated to Stripe
			}
		}

		// ! Process donation information to show on the website
		$is_admin = isset( $payload['admin'] ) && true === $payload['admin'];
		if ( $success || $is_admin ) {

			$admin_flag = ( $is_admin ) ? ' - Admin Operation' : '';

			// Allocate donation
			$donation_info = [
				'date'       => time(),
				'name'       => $payload['userDetails']['fullName'],
				'email'      => $payload['userDetails']['email'],
				'type'       => ( true === $payload['paymentDetails']['monthly'] ) ? 'monthly' : 'one_time',
				'amount'     => $payload['paymentDetails']['amount'],
				'currency'   => $payload['paymentDetails']['currency'],
				'comment'    => ( $payload['donationOptions']['singleStudent'] ) ? 'Direct donation' : 'Randomly allocated from donation to onja.org',
				'deleteThis' => $is_admin,
			];

			// Add admin flag to comment
			$donation_info['comment'] = $donation_info['comment'] . $admin_flag;

			// ! Figure out student who should be donated
			$student_id = '';
			// -> if chosen on front-end and still not fully funded - choose him
			if ( $payload['donationOptions']['singleStudent'] && ! get_post_meta( $payload['donationOptions']['studentID'], 'onja_donation_status', true ) ) {
				$student_id = $payload['donationOptions']['studentID'];
			}

			// -> if direct donation to Onja.org : select student randomly
			if ( ! $payload['donationOptions']['singleStudent'] ) {
				// -> find randomly the non-funded student
				$try_student_id = self::get_random_not_funded_students();
				if ( false !== $try_student_id && is_array( $try_student_id ) ) {
					$student_id = $try_student_id[0];
				}
			}

			if ( $student_id ) {
				self::update_student_donation_meta( $student_id, $donation_info );
			} else {
				$headers = array( 'Content-Type: text/html; charset=UTF-8' );
				wp_mail(
					get_option( 'admin_email' ),
					'Onja Donation Module',
					'Coudn\'t find an unfunded student. The donation was not saved in the database. Donation information: <br>' . print_r( $donation_info ),
					$headers
				);
			}

			// ! Subscribe user to Investors list
			if ( isset( $payload['userDetails']['subscribe'] ) && true === $payload['userDetails']['subscribe'] ) {
				// ! Subscribe to Mailchimp "Investors" list
				$amount = ( true === $payload['paymentDetails']['monthly'] ) ? $payload['paymentDetails']['amount'] * ( self::$donation_period ) : $payload['paymentDetails']['amount'];
				ON_Mailchimp::subscribe_investor_list(
					$payload['userDetails']['email'],
					array(
						'FNAME'   => $payload['userDetails']['fullName'],
						'MMERGE3' => $payload['paymentDetails']['currency'] . ' ' . $amount,
						'MMERGE2' => ( true === $payload['paymentDetails']['monthly'] ) ? 'Monthly donation of ' . $payload['paymentDetails']['amount'] . ' ' . $payload['paymentDetails']['currency'] : 'One time donation',
					)
				);
				// ! Remove from Mailchimp "Interested" list
				ON_Mailchimp::unsubscribe_user( $payload['userDetails']['email'], ON_Mailchimp::$interested_list );
			}

			wp_send_json_success();
		} else {
			wp_send_json_error( $error );
		}
	}

	/**
	 * Update Donation Info
	 *
	 * @param string $student_id
	 * @param array $donation_info
	 * @return void
	 */
	public static function update_donation_info_table( $student_id, $donation_info ) {

		$previous_donation_info = ( json_decode( get_post_meta( $student_id, 'onja_donation_info', true ), true ) ) ?: [];

		$previous_donation_info[] = $donation_info;

		$new_donation_info = json_encode( $previous_donation_info, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );

		if ( $new_donation_info ) {
			update_post_meta( $student_id, 'onja_donation_info', $new_donation_info );
		}
	}

	/**
	 * Get Currency Conversion Info
	 *
	 * @return void
	 */
	public static function get_conversion_info() {

		// Attempt to get info from transient
		$currency_info = get_transient( 'onja_currency_info' );

		if ( ! empty( $currency_info ) ) {
			return $currency_info;
		} else {
			$get_currency_convertion = wp_remote_get( 'http://data.fixer.io/api/latest?access_key=c3a8cbbf2cc0cf24989842d174b3ecb1&symbols=USD,NZD' );
			$response                = json_decode( wp_remote_retrieve_body( $get_currency_convertion ), true );
			if ( true === $response['success'] ) {
				set_transient( 'onja_currency_info', $response, DAY_IN_SECONDS );
				return $response;
			}
		}

		return false;
	}

	/**
	 * Update Full amount sum
	 *
	 * @param string $student_id
	 * @param array $donation_info
	 * @return void
	 */
	public static function update_student_donated_sum( $student_id, $donation_info ) {
		$how_much_to_add = 0;

		// calculate everything in USD
		if ( 'nzd' === strtolower( $donation_info['currency'] ) ) {
			$response = self::get_conversion_info();
			if ( false !== $response && true === $response['success'] ) {
				$rate                    = $response['rates']['USD'] / $response['rates']['NZD'];
				$donation_info['amount'] = $donation_info['amount'] * $rate;
			}
		}

		// add sum to total student donation sum
		if ( 'one_time' === $donation_info['type'] ) {
			$how_much_to_add = $donation_info['amount'];
		} else {
			$how_much_to_add = $donation_info['amount'] * self::$donation_period;
		}

		// check if current student donation full sum will reach the limit
		$current_donation_status = get_post_meta( $student_id, 'onja_donation_sum', true ) + 0;
		$amount_to_goal          = self::$donation_goal - $current_donation_status;

		// ! If donation is more then needed for this student
		if ( $amount_to_goal <= $how_much_to_add ) {
			// -> add amount to goal to current student
			update_post_meta( $student_id, 'onja_donation_sum', ( $current_donation_status + $amount_to_goal ) );
			// -> flag current student as fully funded
			update_post_meta( $student_id, 'onja_donation_status', true );

			// if we have some rest -> allocate to another student
			if ( $amount_to_goal !== $how_much_to_add ) {
				// get random student
				$random_studnet_ids = self::get_random_not_funded_students();
				$random_studnet_id  = '';
				if ( false !== $random_studnet_ids && is_array( $random_studnet_ids ) ) {
					$random_studnet_id = $random_studnet_ids[0];
				}

				// update donation info input
				$rest                     = $how_much_to_add - $amount_to_goal;
				$adjusted_to_monthly_rest = ( 'monthly' === $donation_info['type'] ) ? $rest / ( self::$donation_period ) : $rest;
				$donation_info['amount']  = $adjusted_to_monthly_rest;
				$donation_info['comment'] = 'Randomly allocated from donation to ' . get_the_title( $student_id );

				// update donation meta
				self::update_student_donation_meta( $random_studnet_id, $donation_info );
			}
		} else {
			update_post_meta( $student_id, 'onja_donation_sum', ( $current_donation_status + $how_much_to_add ) );
		}
	}

	/**
	 * Update Student Donation Meta
	 *
	 * @param integer $student_id
	 * @param array $donation_info
	 * @return void
	 */
	public static function update_student_donation_meta( $student_id, $donation_info ) {

		// * update full sum
		self::update_student_donated_sum( $student_id, $donation_info );

		// * log donation info to donation table
		self::update_donation_info_table( $student_id, $donation_info );
	}

	/**
	 * Process Recurring Payment Subscription
	 *
	 * @param array $payload Front-End POST request
	 * @return void
	 */
	public static function process_recurring_payment( $payload ) {
		// * Create a Customer
		$customer = \Stripe\Customer::create(
			[
				'source'  => $payload['token']['id'],
				'email'   => $payload['userDetails']['email'],
				'name'    => $payload['userDetails']['fullName'],
				'address' => [
					'line1'   => $payload['userDetails']['address'],
					'city'    => $payload['userDetails']['city'],
					'country' => $payload['userDetails']['country'],
				],
			]
		);

		// * Subscribe the customer to the plan

		// -> check if cancelation specified
		$end_time            = ( $payload['paymentDetails']['autoCancel'] ) ? strtotime( '+' . self::$donation_period . ' month', time() ) : null;
		$monthly_receipt_num = self::generate_invoice_num();

        $stripe_live_mode = get_field( 'testing_mode__live_mode', 'options' );
        if ( $stripe_live_mode ) {
            $nzd_subscription_plan_id = get_field( 'donation_module_settings', 'options' )['nzd_subscription_plan_id'];
            $usd_subscription_plan_id = get_field( 'donation_module_settings', 'options' )['usd_subscription_plan_id'];
        } else {
            $nzd_subscription_plan_id = get_field( 'nzd_subscription_plan_id_test', 'options' );
            $usd_subscription_plan_id = get_field( 'usd_subscription_plan_id_test', 'options' );
        }

		// -> subscribe user
		$subscription = \Stripe\Subscription::create(
			[
				'customer'  => $customer->id,
				'items'     => [
					[
						'plan'     => ( 'nzd' === strtolower( $payload['paymentDetails']['currency'] ) ) ? $nzd_subscription_plan_id : $usd_subscription_plan_id,
						'quantity' => $payload['paymentDetails']['amount'],
					],
				],
				'cancel_at' => $end_time,
				'metadata'  => [
					'submitted_from'     => $payload['url'],
					'donation_to'        => ( $payload['donationOptions']['singleStudent'] ) ? $payload['donationOptions']['studentName'] : 'organization',
					'with_end_date'      => ( $end_time ) ? 'yes' : 'no',
					'end_date'           => ( $end_time ) ? date( 'd.m.Y H:i', $end_time ) : 'n/a (continuous subscription)',
					'latest_receipt_num' => $monthly_receipt_num,
				],
			]
		);

		// * Send Confirmation Email to donor about successful subscription
		// -> Get Mailjet connection config
		$config = get_field( 'mailjet_config', 'options' );
		ON_Mailjet::send_mailjet_transactional_email_payment(
			array(
				'monthly_first_email'   => true,
				'subscription_end_date' => $subscription['cancel_at'],
				'donor_name'            => $payload['userDetails']['fullName'],
				'donor_address'         => $payload['userDetails']['address'],
				'donor_country'         => $payload['userDetails']['country'],
				'donor_city'            => $payload['userDetails']['city'],
				'receipt_email'         => $payload['userDetails']['email'],
				'receipt_number'        => $monthly_receipt_num,
				'donation_amount'       => $subscription['quantity'] * $subscription['plan']['amount'],
				'donation_currency'     => $subscription['plan']['currency'],
				'donation_date'         => $subscription['created'],
			),
			(int) $config['monthly_first_id']
		);

		return $subscription;
	}

	/**
	 * Process One Time Payments
	 *
	 * @param array $payload Front-end POST params
	 * @return void
	 */
	public static function process_one_time_payment( $payload ) {

		// * Generate Invoice Number
		$one_time_receipt_num = self::generate_invoice_num();

		// * Create Charge (Stripe)
		$payment = \Stripe\Charge::create(
			[
				'amount'               => $payload['paymentDetails']['amount'] * 100,
				'currency'             => strtolower( $payload['paymentDetails']['currency'] ),
				'description'          => ( $payload['donationOptions']['singleStudent'] ) ? 'Donation to ' . $payload['donationOptions']['studentName'] : 'Donation to onja.org',
				'metadata'             => [
					'submitted_from' => $payload['url'],
					'donation_to'    => ( $payload['donationOptions']['singleStudent'] ) ? $payload['donationOptions']['studentName'] : 'organization',
					'receipt_number' => $one_time_receipt_num,
				],
				'receipt_email'        => $payload['userDetails']['email'],
				'source'               => $payload['token']['id'],
				'statement_descriptor' => 'DONATION',
			]
		);

		// * Send Confirmation Email to donor
		// -> Get Mailjet connection config
		$config = get_field( 'mailjet_config', 'options' );
		// -> Send Email
		ON_Mailjet::send_mailjet_transactional_email_payment(
			array(
				'donor_name'        => $payment['billing_details']['name'],
				'donor_address'     => $payload['userDetails']['address'],
				'donor_country'     => $payload['userDetails']['country'],
				'donor_city'        => $payload['userDetails']['city'],
				'receipt_email'     => $payload['userDetails']['email'],
				'receipt_number'    => $one_time_receipt_num,
				'donation_amount'   => $payment['amount'],
				'donation_currency' => $payment['currency'],
				'donation_date'     => $payment['created'],
			),
			(int) $config['one_time_id']
		);

		return $payment;
	}

	/**
	 * Add donation scripts
	 *
	 * @return void
	 */
	public function enqueue_donation_assets() {

		wp_enqueue_script( 'stripe', 'https://js.stripe.com/v3/', array(), null, true );

		$app_script = get_stylesheet_directory() . '/assets/dist/js/index.js';
		wp_enqueue_script( 'donation-app', get_stylesheet_directory_uri() . '/assets/dist/js/index.js', array( 'jquery', 'stripe' ), filemtime( $app_script ), true );

        // Set rate currency
        $stripe_live_mode = get_field( 'testing_mode__live_mode', 'options' );
        if ( $stripe_live_mode ) {
            $stripe_api_pk = get_field( 'stripe_api', 'options' );
        } else {
            $stripe_api_pk = get_field( 'stripe_api_test', 'options' );
        }

		$app_vars = [
			'nonce'         => wp_create_nonce( 'donation' ),
			'ajax_url'      => admin_url( 'admin-ajax.php' ),
			'stripeAPI'     => base64_encode( $stripe_api_pk ),
			'currency_info' => self::get_conversion_info(),
			'text'          => get_field( 'onja_strings', 'options' ),
		];

		wp_localize_script( 'donation-app', 'onja_donation_vars', $app_vars );

	}

	/**
	 * Output Donation Anchor
	 *
	 * @return void
	 */
	public static function show_donation_module() {
		return '<div class="onjaDonation text-center"></div>';
	}

	/**
	 * Output donation info anchor
	 *
	 * @return void
	 */
	public static function show_student_donation_info() {

		// Don't display info if module disabled
		if ( ! get_field( 'donation_module', 'options' ) ) {
			return;
		}

		$status  = ( get_post_meta( get_the_ID(), 'onja_donation_status', true ) ) ?: '0';
		$sum     = round( (float) get_post_meta( get_the_ID(), 'onja_donation_sum', true ) / (int) ( self::$donation_period ), 0 );
		$togo    = round( (float) ( (int) self::$monthly_goal - $sum ), 0 );
		$percent = round( (float) ( ( $sum / (int) self::$monthly_goal ) * 100 ), 0 );

		if ( current_user_can( 'administrator' ) ) {
			echo '<p class="flex flex-row flex-wrap justify-between pb-3 studentActions hidden">
				<span>
					<a href="#" class="JS_admin_reset_donation no-underline flex items-center text-imperial-primer hover:text-water-raceway" data-studentID="' . esc_attr( get_the_ID() ) . '"><i class="icon-back-in-time mr-2 text-water-raceway text-base"></i>' . esc_html( get_field( 'onja_strings', 'options' )['di_remove_admin_donations'] ) . '</a>
					<a href="#" class="JS_admin_delete_donation no-underline flex items-center text-imperial-primer hover:text-water-raceway" data-studentID="' . esc_attr( get_the_ID() ) . '"><i class="icon-trash
					mr-2 text-water-raceway text-base"></i>' . esc_html( get_field( 'onja_strings', 'options' )['di_remove_donations'] ) . '</a>
					<a href="#" class="JS_show_donations_history no-underline flex items-center text-imperial-primer hover:text-water-raceway" ><i class="dashicons dashicons-list-view text-water-raceway text-base"></i> View Donations</a>
				</span>
				<a href="#" class="JS_admin_add_donation no-underline flex items-center text-imperial-primer hover:text-water-raceway"><i class="icon-plus mr-2 text-water-raceway"></i>' . esc_html( get_field( 'onja_strings', 'options' )['di_add_donation'] ) . '</a>
			</p>';
		}

		echo '<div class="donationInfo hidden" data-status="' . esc_attr( $status ) . '" data-sum="' . esc_attr( $sum ) . '">';

			echo '<div class="flex flex-wrap flex-row items-center justify-between">';
				echo "<h5>{$percent}% " . esc_html( get_field( 'onja_strings', 'options' )['donation_info_funded_title'] ) . '</h5>';
			echo '</div>';
			echo "<div class='progressBar'><span class='progressBar__percent' style='width: {$percent}%;'></span></div>";
			echo '<div class="flex flex-row items-center justify-between">';
				echo '<div class="donationInfo__raised"><span>' . esc_html( get_field( 'onja_strings', 'options' )['donation_info_raised'] ) . '</span><span>$' . esc_html( $sum ) . '</span></div>';
				echo '<div class="donationInfo__togo"><span>' . esc_html( get_field( 'onja_strings', 'options' )['donation_info_togo'] ) . '</span><span>$' . esc_html( $togo ) . '</span></div>';
			echo '</div>';

		echo '</div>';

	}

	/**
	 * Show Student Donate Form
	 *
	 * @return void
	 */
	public static function show_student_donate() {

		if ( ! get_field( 'donation_module', 'options' ) ) {
			return;
		}

		$status = get_post_meta( get_the_ID(), 'onja_donation_status', true );
		echo '<div class="studentDonate hidden" data-onja-status="' . esc_attr( $status ) . '"></div>';
	}

	/**
 * Get random not funded students
 *
 * @param integer $limit Number of students to retrieve
 * @return void
 */
	public static function get_random_not_funded_students( $limit = 1 ) {
		$args = [
			'post_type'      => array( 'student' ),
			'post_status'    => 'publish',
			'posts_per_page' => $limit,
			'orderby'        => 'rand',
			'fields'         => 'ids',
			'meta_query'     => [
				'relation' => 'OR',
				[
					'key'     => 'onja_donation_status',
					'compare' => 'NOT EXISTS',
				],
				[
					'relation' => 'AND',
					[
						'key'     => 'onja_donation_status',
						'compare' => 'EXISTS',
					],
					[
						'key'     => 'onja_donation_status',
						'compare' => '!=',
						'value'   => '1',
					],
				],
			],
		];

		$students = new WP_Query( $args );

		if ( is_wp_error( $students ) ) {
			return false;
		}

		return $students->posts;
	}
}
new ON_Donation_Module();
