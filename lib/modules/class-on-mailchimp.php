<?php
/**
 * This class manages the Mailchimp integration of the Onja Pro theme
 *
 * @package Onja Pro\Modules
 */

class ON_Mailchimp {

	/**
	 * Mailchimp API Key
	 */
	static $key = '';

	/**
	 * Potential Client List ID
	 */
	static $potential_client_list;

	/**
	 * Interested List ID
	 */
	static $interested_list;

	/**
	 * Investors list
	 */
	static $investors_list;

	// On class init
	public function __construct() {

		self::$key                   = get_field( 'mailchimp_api', 'options' );
		self::$potential_client_list = get_field( 'potential_client_list', 'options' );
		self::$interested_list       = get_field( 'interested_list', 'options' );
		self::$investors_list        = get_field( 'investors_list', 'options' );

		add_action(
			'ninja_forms_after_submission',
			function( $form_data ) {

				if ( $form_data['form_id'] === '1' ) {
					self::process_contact_form( $form_data );
				}

				if ( $form_data['form_id'] === '2' ) {
					self::process_interest_form( $form_data );
				}

				if ( $form_data['form_id'] === '3' ) {
                    self::process_subscribe_form( $form_data );
				}

			}
		);
	}

	/**
	 * Unsubscribe user from any list
	 *
	 * @param string $email
	 * @param string $list_id
	 * @return void
	 */
	public static function unsubscribe_user( $email, $list_id ) {
		$user_hash = md5( strtolower( $email ) );
		$endpoint  = self::get_members_endpoint( $list_id ) . $user_hash;

		wp_remote_post(
			$endpoint,
			array(
				'method'   => 'PATCH',
				'blocking' => false,
				'headers'  => [
					'Authorization' => 'apikey ' . self::$key,
					'Content-Type'  => 'application/json',
				],
				'body'     => json_encode(
					[
						'email_address' => $email,
						'status'        => 'unsubscribed',
					]
				),
			)
		);
	}

	/**
	 * Subscribe to investor list
	 *
	 * @param string $email
	 * @param array $merge_fields
	 * @return void
	 */
	public static function subscribe_investor_list( $email, $merge_fields ) {

		// $merge_fields_ref = [
		// 	'FNAME' => $first_name,
		// 	'MMERGE4' => $last_name,
		// 	'MMERGE3' => $amount,
		// ];

		$endpoint = self::get_members_endpoint( self::$investors_list );
		wp_remote_post(
			$endpoint,
			array(
				'method'   => 'POST',
				'blocking' => false,
				'headers'  => [
					'Authorization' => 'apikey ' . self::$key,
					'Content-Type'  => 'application/json',
				],
				'body'     => json_encode(
					[
						'email_address' => $email,
						'status'        => 'subscribed',
						'merge_fields'  => $merge_fields,
					]
				),
			)
		);
	}

	/**
	 * Send Member to "Potential Client"
	 *
	 * @param string $email
	 * @param array $merge_fields
	 * @return void
	 */
	public static function subscribe_potential_client( $email, $merge_fields ) {
		// send member to "Potential Client" list
		$endpoint = self::get_members_endpoint( self::$potential_client_list );
		wp_remote_post(
			$endpoint,
			array(
				'method'   => 'POST',
				'blocking' => false,
				'headers'  => [
					'Authorization' => 'apikey ' . self::$key,
					'Content-Type'  => 'application/json',
				],
				'body'     => json_encode(
					[
						'email_address' => $email,
						'status'        => 'subscribed',
						'merge_fields'  => $merge_fields,
					]
				),
			)
		);
	}

	/**
	 * Subscribe email to "Interested" List
	 *
	 * @param string $email
	 * @param array $merge_fields
	 * @return void
	 */
	public static function subscribe_interested_user( $email, $merge_fields ) {
		$endpoint = self::get_members_endpoint( self::$interested_list );
		wp_remote_post(
			$endpoint,
			array(
				'method'   => 'POST',
				'blocking' => false,
				'headers'  => [
					'Authorization' => 'apikey ' . self::$key,
					'Content-Type'  => 'application/json',
				],
				'body'     => json_encode(
					[
						'email_address' => $email,
						'status'        => 'subscribed',
						'merge_fields'  => $merge_fields,
					]
				),
			)
		);
    }

	/**
	 * Add Subscriber to "Potential Client" List
	 *
	 * @param string $email
	 * @param array $merge_fields
	 * @return void
	 */
	public static function add_to_potential_client( $email, $merge_fields ) {

		$user_hash = md5( strtolower( $email ) );
		$endpoint  = self::get_members_endpoint( self::$potential_client_list ) . $user_hash;

		wp_remote_post(
			$endpoint,
			array(
				'method'   => 'PUT',
				'blocking' => false,
				'headers'  => [
					'Authorization' => 'apikey ' . self::$key,
					'Content-Type'  => 'application/json',
				],
				'body'     => json_encode(
					[
						'email_address' => $email,
                        'status_if_new'        => 'subscribed',
                        "status" => "subscribed",
						'merge_fields'  => $merge_fields,
					]
				),
			)
		);
    }

	/**
	 * Process subscribe form
	 *
	 * @param array $form_data
	 * @return void
	 */
	public static function process_subscribe_form( $form_data ) {
		$merge_fields = [
			'FNAME' => $form_data['fields_by_key']['first_name']['value'],
        ];
        $first_name = $form_data['fields_by_key']['first_name']['value'];
        $email        = $form_data['fields_by_key']['email']['value'];

		// send member to "Interested" list
        self::subscribe_interested_user( $email, $merge_fields );

        if ( !empty( $form_data['fields_by_key']['interested_hiring']['value'] ) || 'interested' === $form_data['fields_by_key']['interested_hiring']['value'][0] ) {
			self::add_to_potential_client( $email, $merge_fields );
        }

		// * Send Confirmational Email
		$mailjet_config = get_field( 'mailjet_config', 'options' );
		// bail if no Mailjet configuration or if template not connected
		if ( ! $mailjet_config || ! isset( $mailjet_config['mj_subscribe_template_id'] ) || empty( $mailjet_config['mj_subscribe_template_id'] ) ) {
			return;
		}
		ON_Mailjet::send_mailjet_subscription_confirmation(
			[
				'first_name'            => $first_name,
				'email'         => $email,
			],
			$mailjet_config['mj_subscribe_template_id']
		);

    }

	/**
	 * Process the interest form
	 *
	 * @param array $form_data
	 * @return void
	 */
	public static function process_interest_form( $form_data ) {

		$merge_fields = [
			'FULLNAME' => $form_data['fields_by_key']['full_name_potential_client']['value'],
			'COMPANY'  => $form_data['fields_by_key']['company_potential_client']['value'],
        ];
        $full_name = $form_data['fields_by_key']['full_name_potential_client']['value'];
		$email        = $form_data['fields_by_key']['email']['value'];

		// send member to "Interested" list
        self::subscribe_interested_user( $email, $merge_fields );

        if ( !empty( $form_data['fields_by_key']['periodic_updates']['value'] ) || 'interested' === $form_data['fields_by_key']['periodic_updates']['value'][0] ) {
			self::add_to_potential_client( $email, $merge_fields );
        }

		// * Send Confirmational Email
		$mailjet_config = get_field( 'mailjet_config', 'options' );
		// bail if no Mailjet configuration or if template not connected
		if ( ! $mailjet_config || ! isset( $mailjet_config['mj_hiredeveloper_template_id'] ) || empty( $mailjet_config['mj_hiredeveloper_template_id'] ) ) {
			return;
		}
		ON_Mailjet::send_mailjet_getintouch_confirmation(
			[
				'full_name'            => $full_name,
				'email'         => $email,
			],
			$mailjet_config['mj_hiredeveloper_template_id']
		);
	}

	/**
	 * Process contact form on Ninja Form submission
	 *
	 * @param array $form_data
	 * @return void
	 */
	public static function process_contact_form( $form_data ) {

		$merge_fields = [
			'FULLNAME' => $form_data['fields_by_key']['full_name_contact']['value'],
			'FNAME'    => $form_data['fields_by_key']['full_name_contact']['value'],
		];
		$email        = $form_data['fields_by_key']['email']['value'];

        if ( !empty( $form_data['fields_by_key']['contact_subscribe']['value'] ) || 'periodic_updates' === $form_data['fields_by_key']['contact_subscribe']['value'][0] ) {
            // send member to "Interested" list
            self::subscribe_interested_user( $email, $merge_fields );
        }

        if ( !empty( $form_data['fields_by_key']['interested_hiring']['value'] ) || 'interested' === $form_data['fields_by_key']['interested_hiring']['value'][0] ) {
            if ( !empty( $form_data['fields_by_key']['contact_subscribe']['value'] ) || 'periodic_updates' === $form_data['fields_by_key']['contact_subscribe']['value'][0] ) {
                self::subscribe_potential_client( $email, $merge_fields );
            }
        }

		// * Send Confirmational Email
		$mailjet_config = get_field( 'mailjet_config', 'options' );
		// bail if no Mailjet configuration or if template not connected
		if ( ! $mailjet_config || ! isset( $mailjet_config['mj_getintouch_template_id'] ) || empty( $mailjet_config['mj_getintouch_template_id'] ) ) {
			return;
		}
		ON_Mailjet::send_mailjet_getintouch_confirmation(
			[
				'full_name'            => $full_name,
				'email'         => $email,
			],
			$mailjet_config['mj_getintouch_template_id']
		);

	}

	/**
	 * Get Members Endpoint
	 *
	 * @param string $list_id List ID
	 * @return void
	 */
	public static function get_members_endpoint( $list_id ) {
		return 'https://us12.api.mailchimp.com/3.0/lists/' . $list_id . '/members/';
	}
}

new ON_Mailchimp();
