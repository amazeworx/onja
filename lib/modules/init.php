<?php
/**
 * This file requires all theme modules
 *
 * @package Onja Pro
 * @author OksanaRomaniv <okskozub@gmail.com>
 */

require_once get_stylesheet_directory() . '/lib/modules/class-on-students.php';
//require_once get_stylesheet_directory() . '/lib/modules/class-on-posts.php';
require_once get_stylesheet_directory() . '/lib/modules/class-on-popups.php';
require_once get_stylesheet_directory() . '/lib/modules/class-on-donation-module.php';
require_once get_stylesheet_directory() . '/lib/modules/class-on-mailjet.php';
require_once get_stylesheet_directory() . '/lib/modules/class-on-mailchimp.php';