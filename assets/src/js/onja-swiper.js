// Recent Posts Swiper
setTimeout(function(){
    var elem = document.getElementsByClassName("recent-posts-swiper");
    if (typeof(elem) != 'undefined' && elem != null) {
        var recentPostsSwiper = new Swiper('.recent-posts-swiper', {
            // Optional parameters
            slidesPerView: 1,
            watchOverflow: true,
            autoplay: {
                delay: 5000,
            },
            //loop: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    //autoplay: false,
                },
                1024: {
                    slidesPerView: 2,
                    //autoplay: false,
                },
                1280: {
                    slidesPerView: 3,
                    //autoplay: false,
                },
            }
        })
    }
}, 500);

