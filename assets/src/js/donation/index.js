require('isomorphic-fetch');

import React, { Component } from 'react';
import { render } from 'react-dom';

import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';

import DonationForm from './DonationForm';
import themeSettings from './configs/mui-theme';
import StudentDonate from './studentDonate';

// setup theme
const theme = createMuiTheme(themeSettings);

// Connect to WP version of jquery
const $ = window.jQuery;

class App extends Component {
	constructor(props) {
		super(props);

		// define global module state
		this.state = {
			singleStudent: false,
			studentID: '',
			studentName: '',
		};
	}

	// ! LIFECYCLE
	// Modify Donation Module if on Single Student Page
	componentWillMount() {
        // Warning: componentWillMount has been renamed, and is not recommended for use. See https://fb.me/react-unsafe-component-lifecycles for details.
		// check the body classes to figure out if on the student single pag
		if (true === document.body.classList.contains('single-student')) {
			this.setState({ singleStudent: true });
			this.setState({
				studentID: document.getElementsByClassName('studentHeader')[0].dataset.donationId,
				studentName: document.getElementsByClassName('studentHeader')[0].dataset
					.donationName,
			});
		}
	}

	render() {
		return (
			<>
				<DonationForm donationOptions={this.state} />
			</>
		);
	}
}

// Render Donation Popup
Array.from(document.getElementsByClassName('onjaDonation')).forEach(element => {
	render(
		<MuiThemeProvider theme={theme}>
			<App />
		</MuiThemeProvider>,
		element
	);
});

$(document).on("click", ".donation-popup > a", function (ev) {
	ev.preventDefault();

	$.fancybox.open({
		src: '#donationPopup',
		type: 'inline',
		touch: false,
		clickSlide: false,
		autoFocus: false,
	});

})

// $('.donation-popup a').on('click', ev => {
// 	ev.preventDefault();

//     console.log('clicked');

// 	$.fancybox.open({
// 		src: '#donationPopup',
// 		type: 'inline',
// 		touch: false,
// 		clickSlide: false,
// 		autoFocus: false,
// 	});
// });

// Render Donation Info Module
Array.from(document.getElementsByClassName('studentDonate')).forEach(element => {
	render(
		<MuiThemeProvider theme={theme}>
			<StudentDonate status={element.dataset.onjaStatus} />
		</MuiThemeProvider>,
		element
	);
});

// Add "Reset donation" functionality to admin reset donation link
let adminResetDonationLink = document.querySelector('.JS_admin_reset_donation');
if (adminResetDonationLink) {
	adminResetDonationLink.addEventListener('click', ev => {
		ev.preventDefault();

		if (!confirm(onja_donation_vars.text.reset_donation_confirm)) return;

		$.ajax({
			type: 'post',
			url: onja_donation_vars.ajax_url,
			data: {
				action: 'onja_reset_donation',
				nonce: onja_donation_vars.nonce,
				studentID: ev.currentTarget.dataset.studentid,
			},
			dataType: 'json',
		})
			.done(() => {
				location.reload();
			})
			.fail(jqXHR => {
				alert('Something went wrong! Please try again later');
				console.error(jqXHR);
			});
	});
}

// Add "delete donation" functionality to admin delete donation link
let deleteDonationLink = document.querySelector('.JS_admin_delete_donation');
if (deleteDonationLink) {
	deleteDonationLink.addEventListener('click', ev => {
		ev.preventDefault();

		if (!confirm(onja_donation_vars.text.delete_donations_confirm)) return;

		$.ajax({
			type: 'post',
			url: onja_donation_vars.ajax_url,
			data: {
				action: 'onja_delete_donation',
				nonce: onja_donation_vars.nonce,
				studentID: ev.currentTarget.dataset.studentid,
			},
			dataType: 'json',
		})
			.done(() => {
				location.reload();
			})
			.fail(jqXHR => {
				alert('Something went wrong! Please try again later');
				console.error(jqXHR);
			});
	});
}
