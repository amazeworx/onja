const theme_settings = {
	palette: {
		primary: {
			light: '#5ab2f6',
			main: '#0083c3',
			dark: '#005792',
			contrastText: '#fff',
		},
		secondary: {
			light: '#62e4ff',
			main: '#00b2d8',
			dark: '#0082a6',
			contrastText: '#fff',
		},
	},
	typography: {
		useNextVariants: true,
		fontFamily: ['"Roboto Mono"', 'sans-serif'].join(','),
	},
	shape: {
		borderRadius: 3,
	},
	overrides: {
		MuiOutlinedInput: {
			root: {
				'&:hover:not($disabled):not($focused):not($error) $notchedOutline': {
					borderColor: '#999',
				},
			},
		},
	},
};

export default theme_settings;
