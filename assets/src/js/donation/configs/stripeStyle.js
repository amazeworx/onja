const stripeStyle = {
	base: {
		color: '#213140',
		fontFamily: '"Roboto Mono", sans-serif',
		fontSize: '16px',
		textTransform: 'uppercase',
		'::placeholder': {
			color: '#7d7d7d',
		},
	},
	invalid: {
		color: '#f44336',
	},
};

export default stripeStyle;
