import React, { Component } from 'react';
import { FormControlLabel, Checkbox, Tooltip } from '@material-ui/core';
import { HelpOutlineSharp } from '@material-ui/icons';

// Connect to WP version of jquery
const $ = window.jQuery;

class StudentDonate extends React.Component {
	constructor(props) {
		super(props);

		this.props = props;

		this.state = {
			monthly: true,
			amount: 0,
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick = ev => {
		ev.preventDefault();

		// Open Donation Popup
		$.fancybox.open({
			src: '#donationPopup',
			type: 'inline',
			touch: false,
			clickSlide: false,
			autoFocus: false,
		});
	};

	handleChange = ev => {
		const target = ev.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;

		this.setState({
			[name]: value,
		});
	};

	render() {
		return (
			<>
				<div className="studentDonate__status flex flex-row items-start sm:items-center">
					<div className="studentDonate__amount flex-1">
						<input
							type="number"
							step="1"
							min="0"
							placeholder={onja_donation_vars.text.sform_amount}
							name="amount"
							id="amount"
							className="flex-1"
							onChange={this.handleChange}
						/>
						<a
							href="#"
							className="btn btn--blue btn--narrow"
							onClick={this.handleClick}>
							{onja_donation_vars.text.sform_submit}
						</a>
					</div>
					{this.props.status === '1' && (
						<Tooltip
							placement="right"
							title={onja_donation_vars.text.fully_funded_tooltip}
							className="ml-4 mt-2 sm:mt-0">
							<HelpOutlineSharp />
						</Tooltip>
					)}
				</div>
				<FormControlLabel
					control={
						<Checkbox
							checked={this.state.monthly}
							value="monthly"
							id="monthly"
							name="monthly"
							onChange={this.handleChange}
						/>
					}
					label={onja_donation_vars.text.sform_monthly_toggle}
					className="studentDonate__monthly"
				/>
			</>
		);
	}
}

export default StudentDonate;
