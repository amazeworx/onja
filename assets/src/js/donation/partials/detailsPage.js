import React, { useState } from "react";
import {
  TextField,
  InputLabel,
  FormControlLabel,
  Radio,
  Checkbox
} from "@material-ui/core";

import countries from "../configs/countries";

const DetailsPage = props => {
  const {
    values: { fullName, addressLine1, country, city, email, subscribe },
    errors,
    touched,
    handleChange,
    setFieldTouched,
    isValid,
    btnClass,
    btnLabel,
    navigateToPage,
    admin
  } = props;

  const [isProcessing, setIsProcessing] = useState(false);

  const handleAdminClick = async ev => {
    ev.preventDefault();

    // bail if still processing previous request
    if (isProcessing) {
      return false;
    }

    // start processing request
    setIsProcessing(true);

    let payload = {
      admin,
      paymentDetails: {
        amount: props.values.amount,
        currency: props.values.currency,
        monthly: props.values.monthly
      },
      userDetails: {
        email: props.values.email,
        fullName: props.values.fullName,
        subscribe
      },
      url: window.location.href,
      donationOptions: props.donationOptions
    };

    const response = await fetch(
      onja_donation_vars.ajax_url +
        "?action=onja_create_charge" +
        "&nonce=" +
        onja_donation_vars.nonce,
      {
        method: "POST",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(payload)
      }
    )
      .catch(err => {
        alert("Something went wrong! Please try again later");
      })
      .finally(() => setIsProcessing(false));

    const result = await response.json();

    if (result.success === true) {
      navigateToPage("finished");
      setTimeout(() => {
        location.reload();
      }, 2000);
    } else {
      alert("Something went wrong! Please try again later");
    }
  };

  const handleClick = ev => {
    if (admin) {
      handleAdminClick(ev);
    } else {
      navigateToPage("payment");
    }
  };

  const change = (name, e) => {
    e.persist();
    handleChange(e);
    setFieldTouched(name, true, false);
  };

  return (
    <>
      <div className="detailsPage donationPage">
        <div className="donationRow">
          <div className="w-full mb-6 md:w-1/2 md:mr-3">
            <FormControlLabel
              label={
                admin
                  ? onja_donation_vars.text.form_name_admin
                  : onja_donation_vars.text.form_name
              }
              labelPlacement="top"
              className="donationField"
              htmlFor="fullName"
              control={
                <TextField
                  fullWidth
                  id="fullName"
                  name="fullName"
                  variant="outlined"
                  helperText={touched.fullName ? errors.fullName : ""}
                  error={touched.fullName && Boolean(errors.fullName)}
                  value={fullName}
                  onChange={change.bind(null, "fullName")}
                />
              }
            />
          </div>
          <div className="w-full mb-6 md:w-1/2 md:ml-3">
            <FormControlLabel
              label={onja_donation_vars.text.form_email}
              labelPlacement="top"
              className="donationField"
              htmlFor="email"
              control={
                <TextField
                  fullWidth
                  inputProps={{ type: "email" }}
                  id="email"
                  name="email"
                  variant="outlined"
                  helperText={touched.email ? errors.email : ""}
                  error={touched.email && Boolean(errors.email)}
                  value={email}
                  onChange={change.bind(null, "email")}
                />
              }
            />
          </div>
        </div>
        {!admin && (
          <>
            <div className="donationRow">
              <div className="w-full mb-6">
                <FormControlLabel
                  label={onja_donation_vars.text.form_address}
                  labelPlacement="top"
                  className="donationField"
                  htmlFor="addressLine1"
                  control={
                    <TextField
                      fullWidth
                      id="addressLine1"
                      name="addressLine1"
                      variant="outlined"
                      helperText={
                        touched.addressLine1 ? errors.addressLine1 : ""
                      }
                      error={
                        touched.addressLine1 && Boolean(errors.addressLine1)
                      }
                      value={addressLine1}
                      onChange={change.bind(null, "addressLine1")}
                    />
                  }
                />
              </div>
            </div>
            <div className="donationRow">
              <div className="w-full mb-6 md:w-1/2 md:mr-3">
                <FormControlLabel
                  label={onja_donation_vars.text.form_country}
                  labelPlacement="top"
                  className="donationField"
                  htmlFor="country"
                  control={
                    <TextField
                      fullWidth
                      select
                      id="country"
                      name="country"
                      variant="outlined"
                      autoComplete="country-name"
                      placeholder={
                        onja_donation_vars.text.form_country_placeholder
                      }
                      SelectProps={{ native: true }}
                      helperText={touched.country ? errors.country : ""}
                      error={touched.country && Boolean(errors.country)}
                      value={country}
                      onChange={change.bind(null, "country")}
                    >
                      <option key="" value="" />
                      {countries.map(option => (
                        <option key={option.Code} value={option.Code}>
                          {option.Name}
                        </option>
                      ))}
                    </TextField>
                  }
                />
              </div>
              <div className="w-full mb-6 md:w-1/2 md:ml-3">
                <FormControlLabel
                  label={onja_donation_vars.text.form_city}
                  labelPlacement="top"
                  className="donationField"
                  htmlFor="city"
                  control={
                    <TextField
                      fullWidth
                      id="city"
                      name="city"
                      variant="outlined"
                      helperText={touched.city ? errors.city : ""}
                      error={touched.city && Boolean(errors.city)}
                      value={city}
                      onChange={change.bind(null, "city")}
                    />
                  }
                />
              </div>
            </div>
          </>
        )}
        <div className="donationRow donationRow--subscribe">
          <div className="w-full text-left">
            <FormControlLabel
              control={
                <Checkbox
                  checked={subscribe}
                  value="subscribe"
                  id="subscribe"
                  name="subscribe"
                  onChange={change.bind(null, "subscribe")}
                />
              }
              htmlFor="subscribe"
              label={
                admin
                  ? onja_donation_vars.text.form_subscribe_admin
                  : onja_donation_vars.text.form_subscribe
              }
            />
          </div>
        </div>
      </div>
      <button
        className={btnClass}
        onClick={handleClick}
        disabled={!isValid || isProcessing}
      >
        {btnLabel}
      </button>
    </>
  );
};

export default DetailsPage;
