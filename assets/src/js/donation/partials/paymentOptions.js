import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import {
	FormControlLabel,
	Switch,
	Checkbox,
	Tooltip,
	TextField,
	InputAdornment,
} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { HelpOutlineSharp } from '@material-ui/icons';

const $ = window.jQuery;

const GiveSwitch = withStyles((theme) => ({
    root: {
      width: 32,
      height: 20,
      padding: 0,
      display: 'flex',
    },
    switchBase: {
      padding: 2,
    //   color: theme.palette.grey[500],
      //color: theme.palette.primary.main,
      color: '#0083c3',
      '&$checked': {
        transform: 'translateX(12px)',
        //color: theme.palette.common.white,
        //color: theme.palette.primary.main,
        color: '#0083c3',
        '& + $track': {
          opacity: 1,
        //   backgroundColor: theme.palette.primary.main,
          //borderColor: theme.palette.primary.main,
          backgroundColor: theme.palette.common.white,
          borderColor: theme.palette.grey[500],
        },
      },
    },
    thumb: {
      width: 16,
      height: 16,
      boxShadow: 'none',
    },
    track: {
      border: `1px solid ${theme.palette.grey[500]}`,
      borderRadius: 20 / 2,
      opacity: 1,
      backgroundColor: theme.palette.common.white,
    },
    checked: {},
}))(Switch);

class PaymentOptions extends Component {
	constructor(props) {
		super(props);

		this.props = props;

		this.state = {
			fullyFunded: false,
			singleStudent: false,
			remaining: '',
		};

		this.change = this.change.bind(this);
	}

	change = (name, e) => {
		e.persist();
		this.props.handleChange(e);
		this.props.setFieldTouched(name, true, false);

		// on currency select change
		if (name === 'currency') {
			// only perform conversion when amount specified
			if (this.props.values.amount) {
				const rates = onja_donation_vars.currency_info.rates;
				let converted, remainingConverted, rate;
				if (e.target.value === 'NZD') {
					rate = rates.NZD / rates.USD;
				} else if (e.target.value === 'USD') {
					rate = rates.USD / rates.NZD;
				}
				converted = rate * this.props.values.amount;
				remainingConverted = rate * this.state.remaining;
				this.props.setFieldValue('amount', converted.toFixed(0));
				this.setState({ remaining: remainingConverted.toFixed(0) });
			}
		}
	};

	componentDidMount() {
		const setFieldValue = this.props.setFieldValue.bind(null);

		// Listen for click on student single donation block
		$(document).on('click', '.studentDonate .btn', () => {
			setFieldValue('monthly', $('.studentDonate input[id="monthly"]').prop('checked'));
			setFieldValue('amount', $('.studentDonate input[id="amount"]').val());
		});

		// Listen for header menu dropdown click
		$(document).on('click', '.donation-popup--monthly > a', () => {
            setFieldValue('monthly', true);
		});
		$(document).on('click', '.donation-popup--once > a', () => {
            setFieldValue('monthly', false);
		});

		// set funded state
		if (document.getElementsByClassName('studentHeader').length) {
			let studentHeader = document.getElementsByClassName('studentHeader')[0];
			let donationInfo = document.getElementsByClassName('donationInfo')[0];

			// get funded info
			this.setState({
				fullyFunded: studentHeader.dataset.donationStatus === '1',
			});

			// get remaining amount info
			this.setState({ remaining: 150 - donationInfo.dataset.sum });
		}

		// set single state
		if ($('body').hasClass('single-student')) {
			this.setState({ singleStudent: true });
		}
	}

	componentWillUnmount() {
		$(document).off('change', '.studentDonate input', this.handleStudentFormChange);
	}

	render() {
		return (
			<>
				<div className="donationRow donationRow--monthly justify-center">
                    {/* <label className="label-give-once">Give Once</label>
					<FormControlLabel
						htmlFor="monthly"
						control={
							<Switch
								color="secondary"
								id="monthly"
								name="monthly"
								value="monthly"
                                checked={this.props.values.monthly}
								onChange={this.change.bind(null, 'monthly')}
							/>
						}
						label={
							this.props.admin
								? onja_donation_vars.text.form_give_monthly_switch_admin
								: onja_donation_vars.text.form_give_monthly_switch
						}
					/> */}
                    <Typography component="div">
                        <Grid component="label" container alignItems="center" spacing={1}>
                            <Grid item>
                                <span className={this.props.values.monthly && !this.props.admin ? "" : "text-crayola"}>
                                {
                                    this.props.admin
                                        ? onja_donation_vars.text.popup_title_one_time_admin
                                        : onja_donation_vars.text.popup_title_one_time
                                }
                                </span>
                            </Grid>
                            <Grid item>
                                <GiveSwitch id="monthly" value="monthly" name="monthly" checked={this.props.values.monthly} onChange={this.change.bind(null, 'monthly')} />
                            </Grid>
                            <Grid item>
                                <span className={this.props.values.monthly && !this.props.admin ? "text-crayola" : ""}>
                                {
                                    this.props.admin
                                        ? onja_donation_vars.text.form_give_monthly_switch_admin
                                        : onja_donation_vars.text.form_give_monthly_switch
                                }
                                </span>
                            </Grid>
                        </Grid>
                    </Typography>
				</div>
				<div className="donationRow donationRow--currency justify-center">
					<div className="currencySelect">
						<span className="currencySelect__label">
							{onja_donation_vars.text.form_payment_options_label}
						</span>
						<div className="currencySelect__control">
							<TextField
								value={this.props.values.currency}
								id="currency-select"
								name="currency"
								onChange={this.change.bind(null, 'currency')}
								select
								SelectProps={{
									native: true,
								}}
								margin="none"
								variant="outlined">
								<option key="NZD" value="NZD">
									NZD
								</option>
								<option key="USD" value="USD">
									USD
								</option>
							</TextField>
							<TextField
								helperText={
									this.props.touched.amount ? this.props.errors.amount : ''
								}
								error={
									this.props.touched.amount && Boolean(this.props.errors.amount)
								}
								value={this.props.values.amount}
								onChange={this.change.bind(null, 'amount')}
								id="amount"
								name="amount"
								placeholder="00.00"
								type="number"
								variant="outlined"
								margin="none"
								inputProps={{ min: '0', step: '1' }}
								InputProps={{
									startAdornment: <InputAdornment>$</InputAdornment>,
								}}
							/>
						</div>
						{this.state.singleStudent &&
							!this.state.fullyFunded &&
							this.props.values.monthly && (
								<p className="py-2 leading-tight text-blue-atoll text-left text-sm">
									of ${this.state.remaining} {this.props.values.currency} <br />
									remaining
								</p>
							)}
					</div>
				</div>
				{this.props.values.monthly && !this.props.admin && (
					<div className="donationRecurring pt-4 flex flex-row justify-center items-start">
						<FormControlLabel
							control={
								<Checkbox
									checked={this.props.values.autoCancel}
									onChange={this.change.bind(null, 'autoCancel')}
									value="autoCancel"
									id="autoCancel"
									name="autoCancel"
								/>
							}
							label={onja_donation_vars.text.form_auto_cancel}
						/>{' '}
						<Tooltip
							placement="right"
							title={onja_donation_vars.text.form_cancel_subscription_tooltip}>
							<HelpOutlineSharp />
						</Tooltip>
					</div>
				)}
			</>
		);
	}
}

export default PaymentOptions;
