import React, { Component } from "react";
import { CardElement, injectStripe } from "react-stripe-elements";
import stripeStyle from "../configs/stripeStyle";

class CheckoutForm extends Component {
  constructor(props) {
    super(props);

    this.style = stripeStyle;

    // state
    this.state = {
      stripeErrors: "",
      readyToValidate: false,
      processingPayment: false,
      validating: false
    };

    // methods
    this.handleChange = this.handleChange.bind(this);
    this.submitPayment = this.submitPayment.bind(this);
  }

  /**
   * Handle "Donate" Click
   * @param {event} ev
   */
  async submitPayment(ev) {
    // User clicked donate
    ev.preventDefault();

    // return from function if still submitting
    if (this.state.validating) {
      return false;
    }
    this.setState({ validating: true });

    try {
      // * to get token
      const { token, error } = await this.props.stripe.createToken({
        name: this.props.values.fullName,
        email: this.props.values.email,
        address_line1: this.props.values.addressLine1,
        address_city: this.props.values.city,
        address_country: this.props.values.country
      });

      let payload = {
        token,
        paymentDetails: {
          amount: this.props.values.amount,
          currency: this.props.values.currency,
          monthly: this.props.values.monthly,
          autoCancel: this.props.values.autoCancel
        },
        userDetails: {
          email: this.props.values.email,
          fullName: this.props.values.fullName,
          subscribe: this.props.values.subscribe,
          address: this.props.values.addressLine1,
          country: this.props.values.country,
          city: this.props.values.city
        },
        url: window.location.href,
        donationOptions: this.props.donationOptions
      };
      console.log(payload);

      const response = await fetch(
        onja_donation_vars.ajax_url +
          "?action=onja_create_charge" +
          "&nonce=" +
          onja_donation_vars.nonce,
        {
          method: "POST",
          credentials: "same-origin",
          headers: {
            "Content-Type": "application/json; charset=utf-8"
          },
          body: JSON.stringify(payload)
        }
      );

      const result = await response.json();
      console.log("result :", result);

      if (result.success === true) {
        // handle success
        this.setState({
          stripeErrors: "",
          validating: false,
          readyToValidate: true
        });

        this.props.navigateToPage("finished");
        // setTimeout(() => {
        //   location.reload();
        // }, 3000);
      } else {
        // * handle processed errors
        // display error message
        this.setState({
          stripeErrors: result.data.message,
          validating: false,
          readyToValidate: true
        });
      }
    } catch (error) {
      // * handle errors
      this.setState({
        stripeErrors: error.message,
        readyToValidate: false,
        validating: false
      });
    }
  }

  /**
   * Handle Crad Element Change
   * @param {event} ev
   */
  handleChange(ev) {
    // set the errors
    if (ev.error) {
      this.setState({ stripeErrors: ev.error.message, readyToValidate: false });
    } else {
      this.setState({ stripeErrors: "", readyToValidate: true });
    }
  }

  // ! Lifecycle

  render() {
    let btnClassesLoading = this.state.validating ? "loading" : "";
    let btnClasses = [this.props.btnClass, btnClassesLoading].join(" ");

    return (
      <div className="checkout">
        <CardElement
          className="stripeCard"
          style={this.style}
          onChange={this.handleChange}
        />
        {this.state.stripeErrors && (
          <div id="card-errors" className="text-error" role="alert">
            {this.state.stripeErrors}
          </div>
        )}
        <button
          className={[this.props.btnClass, btnClassesLoading].join(" ")}
          onClick={this.submitPayment}
          disabled={
            !this.props.isValid ||
            !this.state.readyToValidate ||
            this.state.validating
          }
        >
          {this.state.validating
            ? onja_donation_vars.text.button_text_processing
            : this.props.btnLabel}
        </button>
      </div>
    );
  }
}

export default injectStripe(CheckoutForm);
