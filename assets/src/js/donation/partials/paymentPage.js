import React, { Component } from 'react';
import { Elements, StripeProvider } from 'react-stripe-elements';
import CheckoutForm from './checkoutForm';

class PaymentPage extends Component {
	constructor(props) {
		super(props);

		this.props = props;
	}

	render() {
		let key = atob(onja_donation_vars.stripeAPI);
		return (
			<>
				<div className="paymentPage donationPage">
					<StripeProvider apiKey={key}>
						<Elements>
							<CheckoutForm {...this.props} />
						</Elements>
					</StripeProvider>
				</div>
			</>
		);
	}
}

export default PaymentPage;
