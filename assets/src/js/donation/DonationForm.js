import React from 'react';
import { Formik } from 'formik';
import * as Yup from 'yup';

import PaymentOptions from './partials/paymentOptions';
import DetailsPage from './partials/detailsPage';
import PaymentPage from './partials/paymentPage';

// Connect with WP Jquery
const $ = window.jQuery;

const validationSchema = Yup.object({
	amount: Yup.number()
		.positive(onja_donation_vars.text.amount_validation)
		.required(onja_donation_vars.text.amount_is_required),
	fullName: Yup.string().required(onja_donation_vars.text.name_validation),
	addressLine1: Yup.string(),
	country: Yup.string(),
	city: Yup.string(),
	email: Yup.string()
		.email(onja_donation_vars.text.email_validation_invalid)
		.required(onja_donation_vars.text.email_validation_required),
	phone: Yup.string().matches(
		/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/,
		'Enter a valid phone number'
	),
});

class DonationForm extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			page: 'details',
			admin: false,
			singleStudent: false,
		};

		this.initialValues = {
			fullName: '',
			addressLine1: '',
			country: '',
			city: '',
			email: '',
			phone: '',
			subscribe: true,
			monthly: false,
			autoCancel: true,
			currency: 'USD',
			amount: '',
		};

		this.handleSubmit = this.handleSubmit.bind(this);
		this.conditionalLabels = this.conditionalLabels.bind(this);
		this.buildTitle = this.buildTitle.bind(this);
		this.showPage = this.showPage.bind(this);
	}

	// Handle Submit
	handleSubmit(ev) {
		ev.preventDefault();
		ev.currentTarget.blur();

		// proceed to payment
		if (this.state.page === 'details') {
			this.setState({ page: 'payment' });
		}

		// proceed to success
		if (this.state.page === 'payment') {
			this.setState({ page: 'payment' });
		}
	}

	// Show selected page in payment flow
	showPage(pageName) {
		this.setState({ page: pageName });
	}

	// FigureOut Conditional Labels
	conditionalLabels() {
		let btnClass = '';
		let btnLabel = '';
		let formTitle = '';

		if (this.state.page === 'details') {
			btnClass = 'btn btn--blue';
			btnLabel = onja_donation_vars.text.button_text_continue;
			formTitle = onja_donation_vars.text.form_title_details;
		}
		if (this.state.page === 'payment') {
			btnClass = 'btn btn--green';
			btnLabel = onja_donation_vars.text.button_text_donate;
			formTitle = onja_donation_vars.text.form_title_donation;
		}

		return { btnClass, btnLabel, formTitle };
	}

	/**
	 *
	 * @param {object} values Form Values
	 * @param {object} donationOptions Donation Description
	 */
	buildTitle(values, donationOptions) {
		let title = '';

		if (values.monthly) {
			title = this.state.admin
				? onja_donation_vars.text.popup_title_monthly_admin
				: onja_donation_vars.text.popup_title_monthly;
		} else {
			title = this.state.admin
				? onja_donation_vars.text.popup_title_one_time_admin
				: onja_donation_vars.text.popup_title_one_time;
		}

		// Add destination info
		if (donationOptions.singleStudent) {
			title += ` - ${onja_donation_vars.text.popup_title_supporting} ${
				donationOptions.studentName
			}`;
		}

		return title;
	}

	// ! Life Cycle
	componentDidMount() {
		$(document).on('click', '.JS_admin_add_donation', ev => {
			ev.preventDefault();

			this.setState({ admin: true });

			$.fancybox.open({
				src: '#donationPopup',
				type: 'inline',
				touch: false,
				clickSlide: false,
				autoFocus: false,
			});
		});

		// bind fancybox close event
		$(document).on('afterClose.fb', (e, instance, slide) => {
			// clear form
			if (slide.src && slide.src === '#donationPopup') {
                this.resetForm(this.initialValues);
                this.setState({ page: 'details' });
			}
		});
	}

	render(props) {
		return (
			<Formik initialValues={this.initialValues} validationSchema={validationSchema}>
				{props => {
					this.resetForm = props.resetForm.bind(this);
					return (
						<>
                            <h2 className="text-3xl font-extrabold mt-4 mb-10 md:text-4xl md:mb-12">Support Onja</h2>
							{/* <h3 className="text-2xl">{this.buildTitle(props.values, this.props.donationOptions)}</h3> */}
							{this.state.page !== 'finished' && (
								<form className="">
									<PaymentOptions {...props} admin={this.state.admin} />
                                    <div className="donationBody">
                                        <h5 className="formTitle">
                                            {this.conditionalLabels().formTitle}
                                        </h5>
                                        {this.state.page === 'details' && (
                                            <DetailsPage
                                                navigateToPage={this.showPage}
                                                donationOptions={this.props.donationOptions}
                                                btnClass={this.conditionalLabels().btnClass}
                                                btnLabel={
                                                    this.state.admin
                                                        ? onja_donation_vars.text
                                                                .button_text_donate_admin
                                                        : this.conditionalLabels().btnLabel
                                                }
                                                admin={this.state.admin}
                                                {...props}
                                            />
                                        )}
                                        {this.state.page === 'payment' && (
                                            <PaymentPage
                                                navigateToPage={this.showPage}
                                                btnClass={this.conditionalLabels().btnClass}
                                                btnLabel={this.conditionalLabels().btnLabel}
                                                donationOptions={this.props.donationOptions}
                                                {...props}
                                            />
                                        )}
                                    </div>
								</form>
							)}
							{this.state.page === 'finished' && (
								<div className="successPage mx-auto md:px-8">
									<div className="my-12">
                                        {/* <img className="block mx-auto w-20 h-20 md:w-24 md:h24" src="/dev/wp-content/themes/onja/assets/images/finished.svg" /> */}
                                        <img className="block mx-auto w-20 h-20 md:w-24 md:h24" src="https://onja.org/wp-content/themes/onja/assets/images/finished.svg" />
                                    </div>
									<h5 className="text-xl font-bold mb-4">
										{this.state.admin
											? onja_donation_vars.text.popup_title_success_admin
											: onja_donation_vars.text.popup_title_success}
									</h5>
									{!this.state.admin && (
										<p className="text-base max-w-sm">
											{onja_donation_vars.text.popup_content_success}
										</p>
									)}
								</div>
							)}
						</>
					);
				}}
			</Formik>
		);
	}
}

export default DonationForm;
