// Mobile Menu
document.addEventListener(
    "DOMContentLoaded", () => {

        new Mmenu( "#onja-menu", {
            "extensions": [
                "pagedim-black",
                "position-right"
            ],
            "navbars": [
                {
                    "position": "top",
                    "content": [
                        "prev",
                        "title"
                    ]
                }
                ]
            }, {
            // configuration
            offCanvas: {
                clone: true
            }
        });

    }
);