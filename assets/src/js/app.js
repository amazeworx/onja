const $ = window.jQuery;

// CONDITIONAL IMPORT
document.addEventListener("DOMContentLoaded", () => {
    if (
        document.body.contains(document.querySelector(".JS_show_donations_history"))
    ) {
        window.addEventListener("load", () => {
            import(
            /* webpackChunkName: "donationsHistory" */ "./models/donations-history"
            );
        });
    }
});

(function($) {

    // On Popup Show
    $(document).on("afterShow.fb", function(e, instance, slide) {
        // Fill In Email in Subscription Popup
        if (instance.$trigger && instance.$trigger.hasClass("JS_subscribe_popup")) {
            const email = $("#newsletter_email").val();
            $('.onja_popup--newsletter input[type="email"]').val(email);
        }
    });

    // On Popup Close
    $(document).on("afterClose.fb", function(e, instance, slide) {
        // Reset Email in Subscription Popup
        if (instance.$trigger && instance.$trigger.hasClass("JS_subscribe_popup")) {
            $('.onja_popup--newsletter input[type="email"]').val("");
            //$('.onja_popup--newsletter .nf-response-msg').hide();
            //location.reload();
        }
    });

})(jQuery);