import Tabulator from "tabulator-tables";

// DOM
let actionBtn = document.querySelector(".JS_show_donations_history");
let donationsBlock = document.querySelector("#studentDonations");
let tableDiv;
if (donationsBlock) {
  tableDiv = donationsBlock.querySelector("#studentDonations .tableData");
}

if (actionBtn) {
  actionBtn.addEventListener("click", ev => {
    ev.preventDefault();

    donationsBlock.classList.toggle("hidden");

    // DATA
    let tableData = JSON.parse(tableDiv.dataset.donations) || [];

    // INIT Table
    const donationsHistory = new Tabulator(tableDiv, {
      data: tableData,
      layout: "fitDataStretch",
      placeholder: "No Data Available",
      columns: [
        {
          title: "Amount",
          field: "amount",
          formatter: "money",
          formatterParams: {
            decimal: ",",
            thousand: ".",
            precision: 2
          }
        },
        {
          title: "Currency",
          field: "currency"
        },
        {
          title: "Type",
          field: "type",
          formatter: "lookup",
          formatterParams: {
            monthly: "Monthly",
            one_time: "One Time"
          }
        },
        { title: "Name", field: "name", width: 150, formatter: "textarea" },
        {
          title: "Email",
          field: "email",
          formatter: "link",
          formatterParams: {
            labelField: "email",
            urlPrefix: "mailto://",
            target: "_blank"
          }
        },
        {
          title: "Date",
          field: "date",
          formatter: function(cell) {
            let timestamp = cell.getValue();
            let date = new Date(timestamp * 1000);
            return date.toLocaleDateString("nz-NZ", {
              dateStyle: "short",
              timeStyle: "short"
            });
          }
        },
        {
          title: "Description",
          field: "comment",
          formatter: "textarea",
          headerSort: false
        }
      ]
    });

    donationsHistory.redraw();
    donationsBlock.scrollIntoView();
  });
}
