module.exports = {
  future: {
    purgeLayersByDefault: true,
  },
  purge: {
    content: [
        // Paths to your templates...
        "./*.php",
        // "./**/*.php",
        // "./**/**.php",
        // "./**/**/*.php",
        "./templates/*.php",
        "./templates/**/*.php",
        "./lib/*.php",
        "./lib/**/*.php",
        "./lib/**/**/*.php",
        "./assets/src/js/*.js",
        "./assets/src/js/**/*.js",
        "./assets/src/js/**/**/*.js"
    ],
    options: {
        whitelist: ['px-0', 'px-5', 'px-10', 'bg-gray-300'],
    },
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
        colors: {
            'transparent': 'transparent',
            'white': '#fff',
            'gunmetal': '#213140',
            'cerulean': '#00b2d8',
            'crayola': '#0083c3',
            'aliceblue': '#e8ecef',
            'cultured': '#f3f5f7',
            'beaublue': '#c8d1da',
            'blackcoral': '#6a707e',
            'grayweb': '#7d7d7d',
            'tartorange': '#f44336',
            'malachite': '#52d869'
        },
        opacity: {
            '10': '0.1',
            '60': '0.6',
            '80': '0.8',
        },
        backgroundOpacity: {
            '60': '0.6',
            '80': '0.8',
        },
        margin: {
            '7': '1.75rem',
            '9': '2.25rem',
            '14': '3.5rem',
        },
        padding: {
            '7': '1.75rem',
            '9': '2.25rem',
            '14': '3.5rem',
        },
        maxWidth: {
            'prose': '65ch',
        },
        typography: (theme) => ({
            DEFAULT: {
                css: {
                    color: theme('colors.grayweb'),
                    a: {
                        color: theme('colors.crayola'),
                        '&:hover': {
                            color: theme('colors.cerulean'),
                        },
                    },
                    h1: {
                        color: theme('colors.blackcoral'),
                    }
                },
            },
        })
    },
    fontFamily: {
		'nunito': [
            'Nunito Sans',
            'sans-serif'
        ],
    }
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}
