<?php
/**
 * Single
 *
 * @package      Onja
 * @author       Georgius Fransnico
 * @since        1.0.0
 * @license      GPL-2.0+
**/

// This file handles single entries.

// Removes the entry header markup and page title.
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

// Forces full width content layout.
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

// Customize Single Post
if ( is_single() && 'post' == get_post_type() ) {
    add_action( 'genesis_after_header', 'onja_single_hero' );
    add_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
    add_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );
    add_action( 'genesis_entry_header', 'genesis_do_post_title' );
    add_action( 'genesis_entry_header', 'onja_post_thumbnail', '4');
    add_filter( 'genesis_post_info', 'onja_post_info_filter' );
    add_action( 'genesis_entry_header', 'onja_share_buttons', '14' );
    add_filter( 'genesis_post_meta', 'onja_post_meta_filter' );
    add_action( 'genesis_entry_footer', 'onja_post_pagination' );
}

if ( is_single() && 'student' == get_post_type() ) {
    add_action( 'genesis_after_header', 'onja_single_hero' );
}

add_filter( 'genesis_attr_entry', 'onja_post_class' );

// Add hero section
function onja_single_hero() {
    $hero_title = 'Updates';
    $hero_title_tag = 'h3';
    $hero_content = 'Powered by the brilliance of African youth, our scalable model upskills the world’s forgotten talent for exciting careers.';
    $background_image = get_field( 'default_splash', 'options' );
    $overlay = TRUE;
    $height_class = 'h-auto';
    $boxed_class = '';
    $content_position_class = 'text-center max-w-screen-sm mx-auto';

    echo '<section class="section-hero relative flex items-center pt-28 pb-20 px-4 text-white bg-center bg-no-repeat bg-cover md:pt-40 md:pb-20 lg:px-6 xl:px-8 ' . esc_attr( $height_class ) . '" style="background-image:url(' . esc_url( $background_image ) . ')">';

        if ($overlay) {
            echo '<div class="overlay bg-crayola bg-opacity-80 z-0 w-full h-full absolute top-0 left-0 right-0 bottom-0"></div>';
        }

        echo '<div class="container mx-auto z-10 relative flex flex-row flex-wrap">';
            echo '<div class="hero-content ' . esc_attr( $boxed_class ) . ' ' . esc_attr( $content_position_class ) . '">';
                printf( '<%s class="hero-title text-4xl leading-none text-white font-black mb-4 md:text-5xl md:mb-4">%s</%s>', esc_attr( $hero_title_tag ), esc_html( $hero_title ), esc_attr( $hero_title_tag ) );
                if ( ! empty( $hero_content ) ) {
                    echo '<div class="hero-content font-medium text-xl leading-snug md:text-2xl">' . $hero_content . '</div>';
                }
            echo '</div>';
        echo '</div>';

    echo '</section>';
}

//* Add custom classes to posts
function onja_post_class( $attributes ) {

    $attributes['class'] .= ' prose max-w-none mx-auto lg:prose-lg';

    return $attributes;
}

// Add featured image into entry header
function onja_post_thumbnail() {
    if ( ! has_post_thumbnail() ) {
        return;
    }

    echo '<div class="post-featured-image">';
        the_post_thumbnail( 'post-featured', array( 'class' => 'mt-0 lg:mt-0' ) );
    echo '</div>';

}

// Customize entry meta header
function onja_post_info_filter( $post_info ) {
	$post_info = '[post_date]';
	return $post_info;
}

// Add Any Share Buttons
function onja_share_buttons() {
    if ( is_plugin_active('add-to-any/add-to-any.php') ) {
        echo '<div class="entry-share flex items-center"><div class="uppercase text-sm mt-1 mr-2">Share it on</div>' . do_shortcode('[addtoany]') . '</div>';
    }
}

//* Customize the entry meta in the entry footer
function onja_post_meta_filter($post_meta) {
	$post_meta = '[post_categories sep=", " before=""]';
	return $post_meta;
}

//* Add Post Pagination
function onja_post_pagination() {

	if ( ! is_singular( 'post' ) )
		return;

	genesis_markup( array(
		'html5'   => '<div %s>',
		'xhtml'   => '<div class="navigation">',
		'context' => 'adjacent-entry-pagination',
	) );

        echo '<div class="pagination-previous">';
        previous_post_link('%link', 'Back');
        echo '</div>';

        echo '<div class="pagination-next">';
        next_post_link('%link', 'Next');
        echo '</div>';

	echo '</div>';

}


genesis();
