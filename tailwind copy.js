module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
        colors: {
            'transparent': 'transparent',
            'white': '#fff',
            'gunmetal': '#213140',
            'cerulean': '#00b2d8',
            'crayola': '#0083c3',
            'aliceblue': '#e8ecef',
            'cultured': '#f3f5f7',
            'beaublue': '#c8d1da',
            'blackcoral': '#6a707e',
            'grayweb': '#7d7d7d',
            'tartorange': '#f44336'
        },
        typography: (theme) => ({
            DEFAULT: {
                css: {
                    color: 'inherit',
                    h1: { marginTop: 'inherit', marginBottom: 'inherit', color: 'inherit' },
                    h2: { marginTop: 'inherit', marginBottom: 'inherit', color: 'inherit' },
                    h3: { marginTop: 'inherit', marginBottom: 'inherit', color: 'inherit' },
                    h4: { marginTop: 'inherit', marginBottom: 'inherit', color: 'inherit' },
                    h5: { marginTop: 'inherit', marginBottom: 'inherit', color: 'inherit' },
                    h6: { marginTop: 'inherit', marginBottom: 'inherit', color: 'inherit' },
                    p: { marginTop: 'inherit', marginBottom: 'inherit', color: 'inherit' },
                    a: { color: 'inherit', fontWeight: '600' }
                }
            },
            sm: {
                css: {
                    h1: { marginTop: 'inherit' },
                    h2: { marginTop: 'inherit' },
                    h3: { marginTop: 'inherit' },
                    h4: { marginTop: 'inherit' },
                    h5: { marginTop: 'inherit' },
                    h6: { marginTop: 'inherit' },
                    p: { marginTop: 'inherit' },
                }
            },
            md: {
                css: {
                    h1: { marginTop: 'inherit' },
                    h2: { marginTop: 'inherit' },
                    h3: { marginTop: 'inherit' },
                    h4: { marginTop: 'inherit' },
                    h5: { marginTop: 'inherit' },
                    h6: { marginTop: 'inherit' },
                    p: { marginTop: 'inherit' },
                }
            },
            lg: {
                css: {
                    h1: { marginTop: 'inherit' },
                    h2: { marginTop: 'inherit' },
                    h3: { marginTop: 'inherit' },
                    h4: { marginTop: 'inherit' },
                    h5: { marginTop: 'inherit' },
                    h6: { marginTop: 'inherit' },
                    p: { marginTop: 'inherit' },
                }
            },
            xl: {
                css: {
                    h1: { marginTop: 'inherit', marginBottom: 'inherit' },
                    h2: { marginTop: 'inherit', marginBottom: 'inherit' },
                    h3: { marginTop: 'inherit', marginBottom: 'inherit' },
                    h4: { marginTop: 'inherit', marginBottom: 'inherit' },
                    h5: { marginTop: 'inherit', marginBottom: 'inherit' },
                    h6: { marginTop: 'inherit', marginBottom: 'inherit' },
                    p: { marginTop: 'inherit', marginBottom: 'inherit' },
                }
            },
            '2xl': {
                css: {
                    h1: { marginTop: 'inherit' },
                    h2: { marginTop: 'inherit' },
                    h3: { marginTop: 'inherit' },
                    h4: { marginTop: 'inherit' },
                    h5: { marginTop: 'inherit' },
                    h6: { marginTop: 'inherit' },
                    p: { marginTop: 'inherit' },
                }
            }
        })
    },
    fontFamily: {
		'nunito': [
            'Nunito Sans',
            'sans-serif'
        ],
    }
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}
