const mix = require('laravel-mix');
//const tailwindcss = require('tailwindcss');
require('laravel-mix-purgecss');
require('laravel-mix-polyfill');
require('laravel-mix-tailwind');

// Production and Dev ENV configuration
if (!mix.inProduction()) {
    mix
    .webpackConfig({
        devtool: "source-map"
    })
    .sourceMaps();
}

// Settings
mix.options({
    processCssUrls: false,
    postCss: [require("postcss-flexbugs-fixes")()]
});

// Compile assets
mix.tailwind();
mix
    .js("assets/src/js/app.js", "assets/dist/js")
    .js("assets/src/js/swiper-bundle.min.js", "assets/dist/js")
    .js("assets/src/js/onja-mmenu.js", "assets/dist/js")
    .js("assets/src/js/onja-swiper.js", "assets/dist/js")
    .js("assets/src/js/donation/index.js", "assets/dist/js").react()
    .polyfill({
        enabled: true,
        useBuiltIns: "usage",
        targets: "> 2%, last 2 versions, IE 10"
     })
    .webpackConfig({
        output: {
            chunkFilename: "./assets/dist/js/chunks/[name].js?id=[chunkhash]",
            publicPath: "/wp-content/themes/onja/"
        }
    });

mix
    .sass('assets/src/scss/app.scss', 'assets/dist/css')
    .sass('lib/gutenberg/editor-styles.scss', 'lib/gutenberg');