<?php
/**
 * This file renders the "Onja Feature" block
 *
 */

$icon  = get_field( 'icon' );
$title = get_field( 'title' );
$descr = get_field( 'description' );

?>

<div class="feature text-center bg-white-smoke rounded p-6">
	<img src="<?php echo esc_url( $icon ); ?>" alt="<?php echo esc_attr( $title ); ?>">
	<h3 class="my-8 text-lg text-mountain-haze uppercase"><?php echo esc_html( $title ); ?></h3>
	<p class="text-sm"><?php echo wp_kses_post( $descr ); ?></p>
</div>