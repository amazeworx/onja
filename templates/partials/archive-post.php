<?php
/**
 * Archive Post Partial
 *
 * @package Onja
 */

$post_class = get_post_class( 'overflow-hidden rounded-lg bg-white h-full flex flex-col relative', get_the_ID() );

if ( ! has_post_thumbnail() && array_search( 'has-post-thumbnail', $post_class ) ) {
    $key = array_search( 'has-post-thumbnail', $post_class );
    array_splice( $post_class, $key - 1, 1 );
}

$classes = implode( ' ', $post_class );

echo '<div class="' . esc_attr( $classes ) . '">';

    // echo '<span class="rounded-md bg-crayola uppercase text-white text-sm leading-none tracking-wide inline-block px-3 py-2">';
    //     echo '<time datetime="' . get_the_date( 'c' ) . '" itemprop="datePublished">' . get_the_date( 'j M' ) . '</time>';
    // echo '</span>';

    echo '<a class="entry-image-link" href="' . esc_url( get_the_permalink() ) . '">';
        echo get_the_post_thumbnail( $post, 'blog' );
    echo '</a>';

    echo '<div class="pt-6 px-6 pb-8">';
        echo '<h2 class="mb-5 text-lg tracking-wide text-blackcoral font-medium xl:text-xl">';
            echo '<a href="' . esc_url( get_the_permalink() ) . '">' . get_the_title() . '</a>';
        echo '</h2>';
        //echo the_excerpt();
        echo '<div class="font-light text-base xl:text-lg">';
            echo get_excerpt(120);
        echo '</div>';
    echo '</div>';

    echo '<div class="flex flex-row justify-center items-center p-6 mt-auto border-t border-beaublue">';
        echo '<a class="capitalize text-cerulean inline-block leading-none transition hover:text-crayola" href="' . esc_url( get_the_permalink() ) . '">';
            echo '<span class="inline-block leading-none font-medium align-middle">Read More</span> <span class="material-icons align-middle">chevron_right</span>';
        echo '</a>';
	echo '</div>';

echo '</div>';
