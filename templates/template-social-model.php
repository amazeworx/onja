<?php
/**
 * Social Model.
 *
 * Template Name: Social Model
 *
 * @package      Onja
 * @author       Georgius Fransnico
 * @since        1.0.0
 * @license      GPL-2.0+
**/

// Removes the entry header markup and page title.
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

// Forces full width content layout.
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

// Removes the breadcrumbs.
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

/* Remove Loop */
remove_action( 'genesis_loop', 'genesis_do_loop' );

/* Content */
add_action( 'genesis_loop', 'onja_social_model_content' );
function onja_social_model_content() {

    echo onja_do_content_management();

}

genesis();